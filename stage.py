
from datetime import datetime
import database.database as db


def stage_animal_for_sale(user, data):
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_sales"

    header = "REGISTRATION NUMBER,FLOCK,NAME,SEX\n"

    # create string representing string to write to file
    file_string = ""

    # write header to file string
    file_string += header

    should_write_file = False  # assume false to start

    for row in data:
        regnum = row[0]
        flock = row[1]
        name = row[2]
        sex = row[3]


        file_string += f"{regnum}, {flock}, {name}, {sex}\n"
        should_write_file = True

    if should_write_file:
        with open(f"./staging/sales/{filename}.csv", "w") as f:
            f.write(file_string)
        # staging was successful
        return True
    # return false otherwise
    return False


def stage_animal_for_stud(user, data):
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_studs"

    header = "id_animalid, flock_prefix_key, FLOCK,NAME,REGISTRATION NUMBER\n"

    # create string representing string to write to file
    file_string = ""

    # write header to file string
    file_string += header

    should_write_file = False  # assume false to start

    for row in data:
        regnum = row[0]
        flock = row[1]
        name = row[2]

        file_string += f"{flock},{name},{regnum}\n"
        should_write_file = True

    if should_write_file:
        with open(f"./staging/studs/{filename}.csv", "w") as f:
            f.write(file_string)
        # staging was successful
        return True
    # return false otherwise
    return False


def stage_contact_updates(user, data):
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_contact_update"

    header = "FIRST_NAME,MIDDLE_NAME,LAST_NAME,PHONE,EMAIL,PREMISE_NUMBER, ADDRESS_1,ADDRESS_2,CITY,STATE,POSTCODE\n"

    # create string representing string to write to file
    file_string = ""

    # write header to file string
    file_string += header

    should_write_file = False  # assume false to start

    for row in data:

        old_firstname = row[0]
        old_middlename = row[1]
        old_lastname = row[2]
        old_phone = row[3]
        old_email = row[4]
        old_premise_number = row [5]
        old_address_1 = row[6]
        old_address_2 = row[7]
        old_city = row[8]
        old_state = row[9]
        old_post = row[10]

        new_firstname = row[11]
        new_middlename = row[12]
        new_lastname = row[13]
        new_phone = row[14]
        new_email = row[15]
        new_premise_number = row[16]
        new_address_1 = row[17]
        new_address_2 = row[18]
        new_city = row[19]
        new_state = row[20]
        new_post = row[21]

        print("FILE STRING:")


        file_string += f"{old_firstname}, {old_middlename}, {old_lastname}, {old_phone}, {old_email}, {old_premise_number}," \
                       f"{old_address_1}, {old_address_2}, {old_city}, {old_state}, {old_post}\n{new_firstname}, " \
                       f"{new_middlename}, {new_lastname}, {new_phone}, {new_email}, {new_premise_number}, {new_address_1}, " \
                       f"{new_address_2}, {new_city}, {new_state}, {new_post}\n\n"
        print(file_string)
        should_write_file = True

    if should_write_file:
        with open(f"./staging/contacts/{filename}.csv", "w") as f:
            f.write(file_string)
        # staging was successful
        return True
    # return false otherwise
    return False


def stage_transfers(user, data, buyer_contact_id=None, buyer_company_id=None):
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_animal_transfer"

    writeme = "Animal ID, Registration Number, Prefix, Name, Birth Date, Birth Type, Sex, Coat Color\n"

    should_write_file = True
    animal_cont = True
    i = 1
    # animals
    while animal_cont:

        if f"reg_num_{i}" in data.keys():

            animal_id = data[f"animal_id_{i}"]
            regnum = data[f"reg_num_{i}"]
            prefix = data[f"prefix_{i}"]
            name = data[f"name_{i}"]
            dob = data[f"dob_{i}"]
            birth_type = data[f"birth_type_{i}"]
            sex = data[f"sex_{i}"]
            coat_color = data[f"coat_color_{i}"]
            new_row_str = f"{animal_id},{regnum},{prefix},{name},{dob},{birth_type},{sex},{coat_color}\n"
            writeme += new_row_str
            i += 1
        else:
            if i == 1:
                return False  # should not write file, return false
            animal_cont = False

    # get dates animals were sold and moved
    sold_at = data["date-animals-sold"]
    moved_at = data["date-animals-moved"]
    seller_prem = data["seller_prem"]

    # add extra seller data and when the animals moved/sold
    writeme += f"\nSeller ContactID,Seller CompanyID,Seller Premise ID, Sold At,Moved At,\n{user.contact_id},{user.company_id},{seller_prem},{sold_at},{moved_at}\n\n"

    # buyer
    if "nb_address_1" in data.keys():
        # new member
        writeme += "NEW BUYER:\nFirst Name, Last Name, Company, Address 1, Address 2, City, State Key, State,Post Code," \
                   "Federal Scrapie ID (US),Federal Premise ID,State Premise ID,Premise Longitude (Decimal Degrees)," \
                   "Premise Latitude (Decimal Degrees),Primary Phone,Mobile Phone,Primary Email,Website\n"
        # memnum = data["nb_memnum"]
        fn = data["nb_fn"]
        ln = data["nb_ln"]
        company = data["nb_company"]
        address_1 = data["nb_address_1"]
        address_2 = data["nb_address_2"]
        city = data["nb_city"]
        state = data["nb_state"]
        state_key = db.get_state_key(data["nb_state"])
        postcode = data["nb_postcode"]
        fsid = data["nb_fsid"]
        fpid = data["nb_fpid"]
        spid = data["nb_spid"]
        lon = data["nb_lon_dd"]
        lat = data["nb_lat_dd"]
        pp = data["nb_primary_phone"]
        mp = data["nb_mobile_phone"]
        email = data["nb_email"]
        web = data["nb_website"]
        writeme += f"{fn},{ln},{company},{address_1},{address_2},{city},{state_key},{state},{postcode},{fsid},{fpid}," \
                   f"{spid},{lon},{lat},{pp},{mp},{email},{web}\n"
    else:
        # existing member
        memnum = str(data["buyer_memnum"])
        fn = data["buyer_fn"]
        ln = data["buyer_ln"]
        prem= data["buyer_prem"]
        region = data["buyer_region"]
        writeme += f"Existing Member:\nMembership Number,Buyer Contact ID, Buyer Company ID, Buyer Premise ID, First Name,Last Name," \
                   f"Region\n{memnum},{buyer_contact_id},{buyer_company_id},{prem},{fn},{ln},{region}\n"

    # write the file
    try:
        with open(f"staging/transfers/{filename}.csv", 'w') as f:
            f.write(writeme)
        return True
    except Exception as e:
        return False


def staging_offspring_file_creation(user, data: dict):
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_births"
    # todo This really need to be calculated based on owner of dam at time of mating not current owner
    # Need to add in the key for either the contact who is the breeder or the company who is the breeder.

    header = "IS_STILLBORN,ANIMAL_NAME, SEX_KEY,SEX,BIRTHDATE,BIRTH_TYPE_KEY,SIRE_ID,DAM_ID,PREFIX_KEY, PREFIX," \
             "FED_TYPE_KEY, FED_TYPE, FED_COLOR_KEY, FED_COLOR, FED_LOC_KEY, FED_LOC, FED_NUM," \
             "FARM_TYPE_KEY, FARM_TYPE, FARM_COLOR_KEY, FARM_COLOR, FARM_LOC_KEY, FARM_LOC, FARM_NUM," \
             "WEIGHT, WEIGHT_UNITS_KEY, WEIGHT_UNITS," \
             "COAT_COLOR_TABLE_KEY, COAT_COLOR_KEY, COAT_COLOR," \
             "CONCEPTION_TYPE_KEY,CONCEPTION_TYPE, BIRTH_NOTES\n"

    # create string representing string to write to file
    file_string = ""

    # write header to file string
    file_string += header

    cont = True
    i = 0

    should_write_file = False  # assume false to start

    while cont:
        try:
            if f"gender_{i}" in data.keys():

                if f"stillborn_{i}" in data.keys():
                    stillborn = "True"
                else:
                    stillborn = "False"

                sire_id = data['sire_id']
                dam_id = data['dam_id']
                birthdate = data['dob']

                fed_type_key = data[f'type_{i}']
                fed_color_key = data[f'color_{i}']
                farm_tag_type_key = data[f'ft_type_{i}']
                farm_tag_color_key = data[f'ft_color_{i}']

                conception_type_key = data['conception_type_id']
                conception_type = data['conceptionType']
                birth_notes = data['birth_notes']
                birth_type = data["numberBorn"]

                gender = data[f'gender_{i}']
                sex_key = db.get_sex_key(gender)

                prefix = data[f'prefix_{i}']
                prefix_key = db.get_prefix_key(prefix)

                animal_name = data[f'animalName_{i}']
                fed_type = data[f'type_name_{i}']
                fed_color = data[f'color_name_{i}']
                fed_loc = data[f'location_{i}']
                fed_loc_key = db.get_tag_loc_key(fed_loc)
                fed_num = data[f'number_{i}']

                farm_tag_type = data[f'ft_type_name_{i}']

                farm_tag_color = data[f'ft_color_name_{i}']
                if farm_tag_color.startswith("ft_"):
                    farm_tag_color = farm_tag_color[3:]

                farm_tag_loc = data[f'ft_location_{i}']
                if farm_tag_loc.startswith("ft_"):
                    farm_tag_loc = farm_tag_loc[3:]

                farm_tag_loc_key = db.get_tag_loc_key(farm_tag_loc)
                farm_tag_num = data[f'ft_number_{i}']

                weight = data[f'weight_{i}']
                units = data[f'units_{i}']
                weight_units_key = db.get_weight_units_key(units)

                coat_color = data[f'coat_color_{i}']
                coat_color_key = db.get_coat_color_key(coat_color)
                coat_color_table_key = 7

                if stillborn == 'False':
                    # check if any data is missing

                    # check if both farm tag and federal tag are missing
                    if fed_num == '' and farm_tag_num == '':
                        return False

                    if fed_type == '' or fed_color == '' or fed_loc == '' or farm_tag_loc == '' or \
                            farm_tag_type == '' or farm_tag_color == '':
                        return False

                q = [f"{stillborn}", f"{animal_name}", f"{sex_key}", f"{gender}",  f"{birthdate}", f"{birth_type}",
                     f"{sire_id}", f"{dam_id}", f"{prefix_key}", f"{prefix}",
                     f"{fed_type_key}", f"{fed_type}", f"{fed_color_key}", f"{fed_color}",
                     f"{fed_loc_key}", f"{fed_loc}", f"{fed_num}",
                     f"{farm_tag_type_key}", f"{farm_tag_type}", f"{farm_tag_color_key}", f"{farm_tag_color}",
                     f"{farm_tag_loc_key}", f"{farm_tag_loc}", f"{farm_tag_num}",
                     f"{weight}", f"{weight_units_key}", f"{units}",
                     f"{coat_color_table_key}", f"{coat_color_key}", f"{coat_color}",
                     f"{conception_type_key}", f"{conception_type}",
                     f"{birth_notes}"]

                l = ",".join(q) + "\n"

                # add data to file string
                file_string += l

                i += 1

                should_write_file = True  # update once at least 1 whole line is processed
            else:
                cont = False
        except KeyError as e:
            # staging failed
            return False

    if should_write_file:
        with open(f"./staging/births/{filename}.csv", "w") as f:
            f.write(file_string)
        return True
    else:
        return False


def staging_deaths_file_creation(user, data: list):
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_deaths"

    header = "DEATH DATE,ID_ANIMALID,PREFIX_KEY,PREFIX,NAME,REGISTRATION_NUMBER,REASON_KEY,REASON,NOTES\n"

    # create string representing string to write to file
    file_string = ""

    # write header to file string
    file_string += header

    for row in data:

        dod = row[f"dod"]
        # TODO:
        # get id_animalid key#
        id_animalid = row[f"animal_id"]
        # id_animalid = "Need key value here"
        prefix = row[f"prefix"]
        prefix_key = db.get_prefix_key(prefix)
        name = row[f"name"]
        regnum = row[f"regnum"]
        reason = row[f"reason"]
        print (reason)
        reason_key = db.get_death_key(reason)
        notes = row[f"notes"]

        row_string = f"{dod},{id_animalid},{prefix_key},{prefix},{name},{regnum},{reason_key},{reason},{notes}\n"

        file_string += row_string

    should_write_file = len(data) >= 1
    # at end of function call we need to see if we saved the file or not
    if should_write_file:
        with open(f"./staging/deaths/{filename}.csv", "w") as f:
            f.write(file_string)
        return True
    else:
        return False


def staging_id_update_file_creation(user, data):
    # need id_animalinfoid column (primary key)
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_id_update"

    header = "Flock Prefix,Name,Tag Number,Is Official,Type,Color,Location,Date Off,Remove Reason\n"

    # create string representing string to write to file
    file_string = ""

    # write header to file string
    file_string += header

    cont = True
    i = 1

    should_write_file = False  # assume false to start

    while cont:
        try:
            if f"old_name_{i}" in data.keys() and f"new_name_{i}" in data.keys():

                date_removed = data[f"old_date_off_{i}"]
                date_added = data[f"new_date_on_{i}"]

                # old data
                old_prefix = data[f"old_prefix_{i}"]
                old_name = data[f"old_name_{i}"]
                old_tag_num = data[f"old_tagnum_{i}"]
                old_tag_type = data[f"old_type_{i}"]
                old_tag_color = data[f"old_color_{i}"]
                old_tag_loc = data[f"old_loc_{i}"]
                old_reason = data[f"old_reason_{i}"]

                new_prefix = data[f"new_prefix_{i}"]
                new_name = data[f"new_name_{i}"]
                new_tag_num = data[f"new_tagnum_{i}"]
                new_tag_type = data[f"new_type_{i}"]
                new_tag_color = data[f"new_color_{i}"]
                new_tag_loc = data[f"new_loc_{i}"]

                old_is_official = True if f"old_is_official_cb_{i}" in data.keys() else False
                new_is_official = True if f"new_is_official_cb_{i}" in data.keys() else False

                row_string = f"{old_prefix},{old_name},{old_tag_num},{old_is_official},{old_tag_type}," \
                             f"{old_tag_color},{old_tag_loc},{date_removed},{old_reason}\n{new_prefix},{new_name}," \
                             f"{new_tag_num},{new_is_official},{new_tag_type},{new_tag_color}," \
                             f"{new_tag_loc},{date_added}\n\n"

                file_string += row_string

                i += 1

                should_write_file = True  # update to be true once at least 1 whole line is processed
            else:
                cont = False
        except KeyError as e:
            # staging failed
            return False

    # at end of function call we need to see if we saved the file or not
    if should_write_file:
        with open(f"./staging/id_updates/{filename}.csv", "w") as f:
            f.write(file_string)
        return True
    else:
        return False


def stage_registrations(user, data: dict):
    now = datetime.now()

    month = str(now.month)
    if len(month) == 1:
        month = "0" + month

    day = str(now.day)
    if len(day) == 1:
        day = "0" + day

    filename = f"{now.year}-{month}-{day}_{now.hour}-{now.minute}-{now.second}_{user.id}_registrations"

    # header = "DATE,PREFIX,NAME,REGISTRATION NUMBER,REASON,NOTES,FLOCK PREFIX KEY,ID_ANIMALID KEY,DEATH REASON KEY\n"
    header = "ID_ANIMALID,REGISTRATION_NUMBER,ANIMAL_PREFIX,ANIMAL_NAME,BIRTHDATE,SEX,BIRTH_TYPE,IS_OFFICIAL,FED_TYPE_KEY,FED_TYPE," \
             "FED_COLOR_KEY,FED_COLOR,FED_LOC_KEY,FED_LOC,FED_NUM,FARM_TYPE_KEY,FARM_TYPE,FARM_COLOR_KEY,FARM_COLOR," \
             "FARM_LOC_KEY,FARM_LOC,FARM_NUM,COAT_COLOR_KEY,COAT_COLOR\n"

    # create string representing string to write to file
    file_string = ""

    # write header to file string
    file_string += header

    cont = True
    idx = 1
    should_write_file = False

    while cont:
        try:
            id_animalid = data[f"id_animalid_{idx}"]
            reg_num = data[f"reg_num_{idx}"]
            prefix = data[f"prefix_{idx}"]
            name = data[f"name_{idx}"]
            dob = data[f"dob_{idx}"]
            sex = data[f"sex_{idx}"]
            birth_type = data[f"birth_type_{idx}"]

            fed_type_key = data[f"type_{idx}"]
            fed_type_name = data[f"type_name_{idx}"]

            fed_color_key = data[f"color_{idx}"]
            fed_color_name = data[f"color_name_{idx}"]

            fed_location_key = data[f"location_key_{idx}"]
            fed_location_name = data[f"location_name_{idx}"]

            fed_number = data[f"number_{idx}"]

            farm_type_key = data[f"ft_type_{idx}"]
            farm_type_name = data[f"ft_type_name_{idx}"]

            farm_color_key = data[f"ft_color_{idx}"]
            farm_color_name = data[f"ft_color_name_{idx}"]

            farm_location_key = data[f"ft_location_key_{idx}"]
            farm_location_name = data[f"ft_location_name_{idx}"]

            farm_number = data[f"ft_number_{idx}"]

            coat_color_key = data[f"coat_color_key_{idx}"]
            coat_color = data[f"coat_color_{idx}"]

            is_official = data[f"is_official_{idx}"]

            row_string = f"{id_animalid},{reg_num},{prefix},{name},{dob},{sex},{birth_type},{is_official},{fed_type_key}," \
                         f"{fed_type_name},{fed_color_key},{fed_color_name},{fed_location_key},{fed_location_name},{fed_number},{farm_type_key}," \
                         f"{farm_type_name},{farm_color_key},{farm_color_name},{farm_location_key},{farm_location_name},{farm_number},{coat_color_key},{coat_color}\n"
            file_string += row_string
            idx += 1
            should_write_file = True
        except KeyError:
            cont = False

    # at end of function call we need to see if we saved the file or not
    if should_write_file:
        with open(f"./staging/registrations/{filename}.csv", "w") as f:
            f.write(file_string)
        return True
    else:
        return False