from flask import Flask
from flask_login import LoginManager # , current_user
# from flask_session import Session

#import database.database as db

# blueprint import
from apps.app1.views import app1
from apps.app2.auth import auth
from models import User, user_loader_helper


# FIXME see if needed
from flask_session import Session

#from apps.app2.views import app2


def create_app():
    app = Flask(__name__)



    app.config['SESSION_TYPE'] = 'filesystem'
    Session(app)


    # setup with the configuration provided
    app.config.from_object('config.DevelopmentConfig')

    key = "ssecret_key_FIXME"
    app.config['SECRET_KEY'] = key

    # setup all our dependencies
    #database.init_app(app)

    # register blueprint
    app.register_blueprint(app1)
    #app.register_blueprint(auth)
    app.register_blueprint(auth, url_prefix="/app2")

    # DIGITAL OCEAN LINK FOR FLASK:
    #https://www.digitalocean.com/community/tutorials/how-to-add-authentication-to-your-app-with-flask-login

    login_manager = LoginManager()
    login_manager.init_app(app)
    # redirects to this function if user is not logged in when trying to access
    # a "login required" function
    login_manager.login_view = 'auth.login'

    # NOTE:
    # need to specify the "user loader"; this tells flask-login how to find a specific
    # user from the ID that is stored in their session cookie.

    @login_manager.user_loader
    def load_user(user_id):
        # this is super fragile, be careful!
        return user_loader_helper(user_id)

    return app


if __name__ == "__main__":
    create_app().run()
