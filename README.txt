
GENERAL NOTES ABOUT FLASK WEBSITE:

###########################################
# transferring data between screens using flask sessions

General Algorithm to do:

1) user performs a GET request on <page> (ie birth page, id_updates, etc.)

2) user fills in data and hits submit button which does a POST request on same <page>

3) In views.py, if the request method is post, save form data to a session variable
   and then redirect to the staging page where the session data is loaded in
   
4) User *could* make changes at the given page (or not). When they submit they do anther POST
   request to the same page, that information is then stored and then output to a CSV to be
   manually verified.