import sqlite3
from flask import g
import os
from datetime import datetime
from models import User

# PATH TO DATABASE!
DB_PATH = "database/AnimalTrakker_V5_DB.sqlite"


def animal_pedigree(animal_id, depth):
    if depth == 0:
        return "N/A"

    try:

        id_q = """
        SELECT animal_table.id_animalid
         , flock_prefix_table.flock_prefix
         , animal_name
         , sire_id
         , dam_id
         , birth_date
         , id_sexid 
         FROM animal_table 
         INNER JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
         INNER JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
               WHERE animal_table.id_animalid = ?
        """



        # id_q = "SELECT id_animalid, animal_name, sire_id, dam_id, birth_date, id_sexid FROM animal_table " \
        #        "WHERE id_animalid = ?"
        id_animalid, flock_prefix, animal_name, sire_id, dam_id, birth_date, id_sexid = query_db(id_q, args=(animal_id,), one=True)

        reg_num = get_recent_reg_num(animal_id)

    except TypeError:
        id_animalid, animal_name, sire_id, dam_id, birth_date, reg_num, id_sexid = "N/A", "N/A", "N/A", "N/A", "N/A", "N/A", "N/A"
        flock_prefix = ""
    # print("ANS:")
    # print(animal_name, sire_id, dam_id)

    # recursive way of getting data from the database
    data = {'sire': animal_pedigree(sire_id, depth - 1), 'dam': animal_pedigree(dam_id, depth - 1)}

    return {
        "name": animal_name,
        "animal_id": id_animalid,
        "birth_date": birth_date,
        "reg_num": reg_num,
        "parents": data,
        "sex_id": id_sexid,
        "flock_prefix": flock_prefix
    }


def contact_or_company_id(contact_id=None, company_id=None):
    if contact_id is None and company_id is None:
        raise Exception("must pass in a contact or company id.")
    elif contact_id is not None and company_id is not None:
        raise Exception("Cannot pass in both contact_id and company_id to this function.")

    if contact_id is not None:
        return contact_id
    else:
        return company_id

def get_death_key(death_reason):
    q = "SELECT id_deathreasonid FROM death_reason_table WHERE death_reason = ? AND id_companyid = 25"
    result = query_db(q, (death_reason,), one=True)
    return result[0] if result else None


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DB_PATH)
    return db


def get_animals_by_location(mem_id=None):
    q = """
    SELECT
        animal_table.id_animalid
        , (SELECT
            id_number
            FROM animal_id_info_table
            WHERE
                official_id = "1"
                AND animal_id_info_table .id_animalid = animal_table.id_animalid
                AND  (id_date_off IS NULL OR id_date_off = ''))
            AS fedtag
        , (SELECT
             id_number
            FROM animal_id_info_table
            WHERE
                id_idtypeid = "4"
                AND animal_id_info_table.id_animalid = animal_table.id_animalid
                AND  (id_date_off IS NULL OR id_date_off = ''))
            AS farmtag
        , animal_table.animal_name
        , animal_table.birth_date
        , sex_table.sex_abbrev
        , birth_type_table.birth_type
        , membership_number
        , animal_registration_table.registration_number
    FROM
        (SELECT
            id_animalid, MAX(movement_date)
            , to_id_premiseid
            , id_animallocationhistoryid
        FROM animal_location_history_table
        GROUP BY
            id_animalid
        ) AS last_movement_date
    INNER JOIN animal_table ON animal_table.id_animalid = last_movement_date.id_animalid
    INNER JOIN birth_type_table ON animal_table.id_birthtypeid = birth_type_table.id_birthtypeid
    INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
    INNER JOIN owner_registration_table on owner_registration_table.id_contactid = last_movement_date.to_id_premiseid
    LEFT JOIN animal_registration_table ON animal_registration_table.id_animalid = last_movement_date.id_animalid

    WHERE
        animal_table.death_date = ""
        AND  last_movement_date.to_id_premiseid = 1
    --      AND animal_table.id_sexid = 1
    --      AND animal_table.birth_date < "2022%"
    AND owner_registration_table.id_registry_id_companyid = 25

    ORDER BY
        sex_table.sex_abbrev ASC
        , animal_table.birth_date ASC
    """

    ans = query_db(q)

    return ans


def all_alive_registered_animals_owned_by(contact_id=None, company_id=None, get_coat_color=True):
    id_to_check = contact_or_company_id(contact_id, company_id)

    q = f"""
    SELECT animal_table.id_animalid
        , animal_registration_table.registration_number
        , flock_prefix_table.flock_prefix
        , animal_table.animal_name
        , animal_id_info_table.id_number 
        , sex_table.sex_name
        , birth_type_table.birth_type
        , animal_table.birth_date
        FROM
            (SELECT
                id_animalid
                , MAX(transfer_date)
                , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) owner
            FROM animal_ownership_history_table
            GROUP BY
                id_animalid
            ) AS last_transfer_date
        
    INNER JOIN animal_table ON animal_table.id_animalid = last_transfer_date.id_animalid
    INNER JOIN animal_id_info_table ON animal_id_info_table.id_animalid = animal_table.id_animalid
    INNER JOIN id_type_table ON id_type_table.id_idtypeid = animal_id_info_table.id_idtypeid
    INNER JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
    INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
    INNER JOIN birth_type_table ON animal_table.id_birthtypeid = birth_type_table.id_birthtypeid
    INNER JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
    INNER JOIN
        (SELECT animal_registration_table.id_animalid, MAX(animal_registration_table.registration_date) AS registration_date
            FROM animal_registration_table
            WHERE
                animal_registration_table.id_animalregistrationtypeid IN (2 ,6 ,7 ) --registered Black, Chocolate,White Welsh
                AND animal_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
            GROUP BY animal_registration_table.id_animalid
        ) AS current_registration_date ON animal_table.id_animalid = current_registration_date.id_animalid
    INNER JOIN animal_registration_table ON animal_table.id_animalid = animal_registration_table.id_animalid AND animal_registration_table.registration_date = current_registration_date.registration_date
    WHERE 
        animal_id_info_table.id_date_off = '' 
        AND animal_id_info_table.official_id = 1
        AND animal_table.death_date = "" 
        AND  last_transfer_date.owner = ?  -- This is what you will need to pass the query.
    
    ORDER BY 
        sex_table.sex_name ASC
        , animal_registration_table.registration_number
    """

    ans = query_db(q, args=(id_to_check,))

    if get_coat_color:

        coat_q = """
            SELECT
    	        coat_color
            FROM
    	        genetic_coat_color_table
            WHERE
    	        id_geneticcoatcolorid = (SELECT
    								id_geneticcharacteristicvalueid
    							 FROM
    								animal_genetic_characteristic_table
    							 WHERE
    								id_animalid = ?);
            """

        ret = list()

        for row in ans:
            new_row = list(row)
            child_id = row[0]

            color_tup = query_db(coat_q, args=(child_id,), one=True)

            if color_tup is None:
                coat_color = "Unknown"
            else:
                coat_color = color_tup[0]

            new_row.append(coat_color)
            ret.append(new_row)
        return ret

    else:
        return ans


def all_alive_sires_owned_currently(contact_id=None, company_id=None):
    return [x for x in all_alive_registered_animals_owned_by(contact_id, company_id) if x[5].lower() == 'ram']


def all_alive_dams_owned_currently(contact_id=None, company_id=None):
    return [x for x in all_alive_registered_animals_owned_by(contact_id, company_id) if x[5].lower() == 'ewe']


def sires_owned_by_location(mem_id=None):
    q = """
        SELECT
                animal_table.id_animalid
                , (SELECT
                        id_number
                        FROM animal_id_info_table
                        WHERE
                                official_id = "1"
                                AND animal_id_info_table .id_animalid = animal_table.id_animalid
                                AND  (id_date_off IS NULL OR id_date_off = ''))
                        AS fedtag
                , (SELECT
                        id_number
                        FROM animal_id_info_table
                        WHERE
                                id_idtypeid = "4"
                                AND animal_id_info_table.id_animalid = animal_table.id_animalid
                                AND  (id_date_off IS NULL OR id_date_off = ''))
                        AS farmtag
                , animal_table.animal_name
                , animal_table.birth_date
                , sex_table.sex_abbrev
                , birth_type_table.birth_type
           , membership_number
                 FROM
                        (SELECT
                                id_animalid, MAX(movement_date)
                                , to_id_premiseid
                                , id_animallocationhistoryid
                        FROM animal_location_history_table
                        GROUP BY
                                id_animalid
                        ) AS last_movement_date

            INNER JOIN animal_table ON animal_table.id_animalid = last_movement_date.id_animalid
            INNER JOIN birth_type_table ON animal_table.id_birthtypeid = birth_type_table.id_birthtypeid
            INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
            INNER JOIN owner_registration_table on owner_registration_table.id_contactid = last_movement_date.to_id_premiseid

            WHERE
                animal_table.death_date = ""
                AND  last_movement_date.to_id_premiseid = 1
                    AND animal_table.id_sexid = 1
            --      AND animal_table.birth_date < "2022%"
            AND owner_registration_table.id_registry_id_companyid = 25

            ORDER BY
                sex_table.sex_abbrev ASC
                , animal_table.birth_date ASC
        """

    ans = query_db(q)

    return [data[3] for data in ans]


def dams_owned_by(mem_id=None):
    q = """
        SELECT
                animal_table.id_animalid
                , (SELECT
                        id_number
                        FROM animal_id_info_table
                        WHERE
                                official_id = "1"
                                AND animal_id_info_table .id_animalid = animal_table.id_animalid
                                AND  (id_date_off IS NULL OR id_date_off = ''))
                        AS fedtag
                , (SELECT
                        id_number
                        FROM animal_id_info_table
                        WHERE
                                id_idtypeid = "4"
                                AND animal_id_info_table.id_animalid = animal_table.id_animalid
                                AND  (id_date_off IS NULL OR id_date_off = ''))
                        AS farmtag
                , animal_table.animal_name
                , animal_table.birth_date
                , sex_table.sex_abbrev
                , birth_type_table.birth_type
           , membership_number
                 FROM
                        (SELECT
                                id_animalid, MAX(movement_date)
                                , to_id_premiseid
                                , id_animallocationhistoryid
                        FROM animal_location_history_table
                        GROUP BY
                                id_animalid
                        ) AS last_movement_date

            INNER JOIN animal_table ON animal_table.id_animalid = last_movement_date.id_animalid
            INNER JOIN birth_type_table ON animal_table.id_birthtypeid = birth_type_table.id_birthtypeid
            INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
            INNER JOIN owner_registration_table on owner_registration_table.id_contactid = last_movement_date.to_id_premiseid

            WHERE
                animal_table.death_date = ""
                AND  last_movement_date.to_id_premiseid = 1
                    AND animal_table.id_sexid = 2
            --      AND animal_table.birth_date < "2022%"
            AND owner_registration_table.id_registry_id_companyid = 25

            ORDER BY
                sex_table.sex_abbrev ASC
                , animal_table.birth_date ASC
        """

    ans = query_db(q)

    return [data[3] for data in ans]


def death_reasons():
    q = "SELECT death_reason FROM death_reason_table WHERE id_companyid = 700 ORDER BY death_reason_display_order ;"
    reasons = query_db(q)
    return [r[0] for r in reasons]

def death_reasons_no_stillborn():
    q = "SELECT death_reason FROM death_reason_table WHERE id_companyid = 700 ORDER BY death_reason_display_order ;"
    reasons = query_db(q)
    return [r[0] for r in reasons]


def get_all_animals(name=None, reg_num=None, flock=None, coat_color=None, sex_filter=None, only_alive_animals=True, owner_member_number=None):


    q1 = """
     SELECT animal_table.id_animalid
    , animal_registration_table.registration_number
    , flock_prefix_table.flock_prefix
    , animal_table.animal_name
    , animal_table.birth_date
    , animal_table.death_date
    , sex_table.sex_name
    , genetic_coat_color_table.coat_color
    FROM
    animal_table
    INNER JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
    INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
    INNER JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
	INNER JOIN animal_genetic_characteristic_table on  animal_table.id_animalid = animal_genetic_characteristic_table.id_animalid
	INNER JOIN genetic_coat_color_table on animal_genetic_characteristic_table.id_geneticcharacteristicvalueid = genetic_coat_color_table.id_geneticcoatcolorid 
		AND animal_genetic_characteristic_table.id_geneticcharacteristictableid = 7
    INNER JOIN
    (SELECT animal_registration_table.id_animalid, MAX(animal_registration_table.registration_date) AS registration_date
    FROM animal_registration_table
    WHERE
    animal_registration_table.id_animalregistrationtypeid IN (1 ,2 ,4 ,6 ,7 ,10 ,11 ) --registered and BN Black, Chocolate & White Welsh
    AND animal_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
    GROUP BY animal_registration_table.id_animalid
    ) AS current_registration_date ON animal_table.id_animalid = current_registration_date.id_animalid
    INNER JOIN animal_registration_table ON animal_table.id_animalid = animal_registration_table.id_animalid 
		AND animal_registration_table.registration_date = current_registration_date.registration_date
    """

    args = list()
    # add WHERE clauses
    where_params = "WHERE animal_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55) AND animal_flock_prefix_table.id_registry_id_companyid IN ( 25, 54, 55)"


    # '%' + searchstr + '%'
    if name is not None:
        args.append(f"%{name}%")
        where_params += "AND animal_table.animal_name LIKE ?"

    if reg_num is not None:
        args.append(f"%{reg_num}%")
        where_params += "AND animal_registration_table.registration_number LIKE ?"

    if flock is not None:
        args.append(f"%{flock}%")
        where_params += "AND flock_prefix_table.flock_prefix LIKE ?"

    if coat_color is not None:
        args.append(f"%{coat_color}%")
        where_params += "AND genetic_coat_color_table.coat_color LIKE ?"

    if only_alive_animals:
        where_params += "AND animal_table.death_date = \"\""

    # if owner_member_number is not None:
    #     args.append(f"%{owner}%")
    #     # this needs to get the last record in the animal_ownership_history_table and cross reference with the
    #     # member_number from the owner_registration_table for the registries we care about.
    #     where_params += "AND last_transfer_date.owner LIKE ?"

    args = tuple(args)

    q2 = """
        ORDER BY
        animal_registration_table.registration_number
        """

    ans = query_db(q1 + where_params + q2, args=args)

    ret = list()

    for row in ans:
        new_row = list(row)

        animal_id = new_row[0]
        #animal_coat_color = get_coat_color_of_animal(animal_id)
        ret.append(new_row)
        #if coat_color is not None:
            # print(f"DEBUG! -> COAT COLOR:{coat_color}\nACC:{animal_coat_color}\nRESULT -> {coat_color in animal_coat_color}")
         #   if coat_color.lower() in animal_coat_color.lower():
                #new_row.append(animal_coat_color)
                #ret.append(new_row)
        #else:
            #new_row.append(animal_coat_color)
            #ret.append(new_row)

    # filter by sex
    if sex_filter is not None and sex_filter.lower() != "all":
        new_ret = list()
        #6

        filter_val = ""

        # if sex_filter.lower() == "female":
        #     filter_val = "ewe"
        # elif sex_filter.lower() == "male":
        #     filter_val = "ram"
        # elif sex_filter.lower() == "castrate":
        #     filter_val = "wether"

        filter_val = sex_filter.lower()

        for row in ret:
            if row[6].lower() == filter_val:
                new_ret.append(row)

        return new_ret


    return ret


# unused SO FAR
def get_all_registered_animals_owned_by(contact_id=None, company_id=None):
    id_to_check = contact_or_company_id(contact_id, company_id)

    q = """
    SELECT animal_table.id_animalid
    , animal_registration_table.registration_number
    , flock_prefix_table.flock_prefix
    , animal_table.animal_name
    , sex_table.sex_abbrev
    , (SELECT
                       id_number
                       FROM animal_id_info_table
                       WHERE
                               official_id = "1"
                               AND animal_id_info_table .id_animalid = animal_table.id_animalid
                               AND  (id_date_off IS NULL OR id_date_off = ''))
                       AS fedtag
    FROM
    (SELECT
    id_animalid
    , MAX(transfer_date)
    , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) owner
    FROM animal_ownership_history_table
    GROUP BY
    id_animalid
    ) AS last_transfer_date
    INNER JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
    INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
    INNER JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
    INNER JOIN
    (SELECT animal_registration_table.id_animalid, MAX(animal_registration_table.registration_date) AS registration_date
    FROM animal_registration_table
    WHERE
    animal_registration_table.id_animalregistrationtypeid IN (2 ,6 ,7 ) --registered Black, Chocolate,White Welsh
    AND animal_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
    GROUP BY animal_registration_table.id_animalid
    ) AS current_registration_date ON animal_table.id_animalid = current_registration_date.id_animalid
    INNER JOIN animal_registration_table ON animal_table.id_animalid = animal_registration_table.id_animalid AND animal_registration_table.registration_date = current_registration_date.registration_date
    WHERE
    animal_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55)
    AND animal_table.death_date = ""
    AND  last_transfer_date.owner = ?
    ORDER BY
    animal_registration_table.registration_number
    """

    return query_db(q, args=(id_to_check,))


def get_animal_id_info(contact_id=None, company_id=None):
    id_to_check = contact_or_company_id(contact_id, company_id)

    new_q = """
        SELECT animal_table.id_animalid
        , flock_prefix_table.flock_prefix
        , animal_table.animal_name
        , animal_id_info_table.id_animalidinfoid
        , animal_id_info_table.id_number 
        , animal_id_info_table.official_id
        , id_type_table.id_idtypeid 
        , id_color_table.id_color_name
        , id_location_table.id_location_name
        , animal_table.birth_date
        , sex_table.sex_name
        , birth_type_table.birth_type
        , animal_registration_table.registration_number
    
        FROM
            (SELECT
                id_animalid
                , MAX(transfer_date)
                , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) owner
            FROM animal_ownership_history_table
            GROUP BY
                id_animalid
            ) AS last_transfer_date
        
        INNER JOIN animal_table ON animal_table.id_animalid = last_transfer_date.id_animalid
        INNER JOIN animal_id_info_table ON animal_id_info_table.id_animalid = animal_table.id_animalid
        INNER JOIN id_type_table ON id_type_table.id_idtypeid = animal_id_info_table.id_idtypeid
        INNER JOIN id_color_table ON id_color_table.id_idcolorid = animal_id_info_table.id_male_id_idcolorid
        INNER JOIN id_location_table ON id_location_table.id_idlocationid = animal_id_info_table.id_idlocationid
        INNER JOIN birth_type_table ON animal_table.id_birthtypeid = birth_type_table.id_birthtypeid 
        INNER JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
        INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
        INNER JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
        INNER JOIN
            (SELECT animal_registration_table.id_animalid, MAX(animal_registration_table.registration_date) AS registration_date
                FROM animal_registration_table
                WHERE
                    animal_registration_table.id_animalregistrationtypeid IN (1 ,2 ,6 ,7 ,10 ,11) --registered Black, Cocolate,White Welsh
                    AND animal_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
                GROUP BY animal_registration_table.id_animalid
            ) AS current_registration_date ON animal_table.id_animalid = current_registration_date.id_animalid
        INNER JOIN animal_registration_table ON animal_table.id_animalid = animal_registration_table.id_animalid AND animal_registration_table.registration_date = current_registration_date.registration_date
        WHERE 
            animal_id_info_table.id_date_off = '' 
            AND animal_table.death_date = "" 
            AND  id_type_table.id_idtypeid  IN ( 1 ,2 ,3 ,4 ,10 ,11 ,12 ,13)
            AND  last_transfer_date.owner = ?  -- This is what you will need to pass the query.
        ORDER BY 
            animal_table.birth_date ASC
    """
    ans = query_db(new_q, args=(id_to_check,))
    return ans


def get_animal_info_by_owner(contact_id=None, company_id=None, get_coat_colors=True):
    id_to_check = contact_or_company_id(contact_id, company_id)

    q = """
    SELECT DISTINCT animal_table.id_animalid
    , animal_registration_table.registration_number
    , flock_prefix_table.flock_prefix
    , animal_table.animal_name
    , animal_table.birth_date
    , birth_type_table.birth_type
    , sex_table.sex_name
    , (SELECT
        animal_id_info_table.id_number
      FROM animal_id_info_table
        WHERE
            animal_id_info_table.official_id = "1"
            AND animal_id_info_table.id_animalid = animal_table.id_animalid
            AND animal_id_info_table.id_date_off = ''
    LIMIT 1) AS fedtag
    , (SELECT
    id_color_name
    FROM id_color_table
    INNER JOIN animal_id_info_table ON id_color_table.id_idcolorid = animal_id_info_table.id_male_id_idcolorid
    WHERE
    animal_id_info_table.official_id = "1"
    AND animal_id_info_table.id_animalid = animal_table.id_animalid
    AND animal_id_info_table.id_date_off = ''
    LIMIT 1) AS fedtag_color
    , (SELECT
    id_location_name
    FROM id_location_table
    INNER JOIN animal_id_info_table ON id_location_table.id_idlocationid = animal_id_info_table.id_idlocationid
    WHERE
    animal_id_info_table.official_id = "1"
    AND animal_id_info_table.id_animalid = animal_table.id_animalid
    AND animal_id_info_table.id_date_off = ''
    LIMIT 1) AS fedtag_location
    
    , (SELECT
    animal_id_info_table.id_number
      FROM animal_id_info_table
        WHERE
    id_idtypeid = 4 -- farm tags are defined as type 4 in this database
    AND animal_id_info_table.id_animalid = animal_table.id_animalid
    AND animal_id_info_table.id_date_off = ''
    LIMIT 1) AS farmtag
    
    , (SELECT
    id_color_name
    FROM id_color_table
    INNER JOIN animal_id_info_table on id_color_table.id_idcolorid = animal_id_info_table.id_male_id_idcolorid
    WHERE
    id_idtypeid = 4
    AND animal_id_info_table.id_animalid = animal_table.id_animalid
    AND animal_id_info_table.id_date_off = ''
    LIMIT 1) AS farmtag_color
    
    , (SELECT
    id_location_name
    FROM id_location_table
    INNER JOIN animal_id_info_table ON id_location_table.id_idlocationid = animal_id_info_table.id_idlocationid
    WHERE
    id_idtypeid = 4
    AND animal_id_info_table.id_animalid = animal_table.id_animalid
    AND animal_id_info_table.id_date_off = ''
    LIMIT 1) AS farmtag_location
    
           FROM
               (SELECT
                   id_animalid
                   , MAX(transfer_date)
                   , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) owner
               FROM animal_ownership_history_table
               GROUP BY
                   id_animalid
               ) AS last_transfer_date
    
       INNER JOIN animal_table ON animal_table.id_animalid = last_transfer_date.id_animalid
       INNER JOIN animal_id_info_table ON animal_id_info_table.id_animalid = animal_table.id_animalid
       INNER JOIN id_type_table ON id_type_table.id_idtypeid = animal_id_info_table.id_idtypeid
       INNER JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
       INNER JOIN id_color_table ON id_color_table.id_idcolorid = animal_id_info_table.id_male_id_idcolorid
       INNER JOIN id_location_table ON id_location_table.id_idlocationid = animal_id_info_table.id_idlocationid
       INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
       INNER JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
       INNER JOIN birth_type_table ON animal_table.id_birthtypeid = birth_type_table.id_birthtypeid
       INNER JOIN
           (SELECT animal_registration_table.id_animalid, MAX(animal_registration_table.registration_date) AS registration_date
               FROM animal_registration_table
               WHERE
                   animal_registration_table.id_animalregistrationtypeid IN (1 ,2 ,4 ,6 ,7 ,10 ,11) --all Black, Chocolate, White Welsh
                   AND animal_registration_table.id_registry_id_companyid IN (25 ,54 ,55) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
               GROUP BY animal_registration_table.id_animalid
           ) AS current_registration_date ON animal_table.id_animalid = current_registration_date.id_animalid
       INNER JOIN animal_registration_table ON animal_table.id_animalid = animal_registration_table.id_animalid AND animal_registration_table.registration_date = current_registration_date.registration_date
       WHERE
           animal_id_info_table.id_date_off = ''
           --AND animal_id_info_table.official_id = 1
           AND animal_table.death_date = ''
           AND  last_transfer_date.owner = ?  -- This is what you will need to pass the query.
    
       ORDER BY
       animal_registration_table.registration_number
    """

    ans = query_db(q, args=(id_to_check,))

    if get_coat_colors:

        ret = list()

        for row in ans:

            new_row = list(row)
            child_id = row[0]

            coat_color = get_coat_color_of_animal(child_id)
            #name = row[3]  # REMOVE ME
            new_row.append(coat_color)
            ret.append(new_row)
        return ret

    else:
        return ans


def get_birth_notified_animals(member_number):
    try:
        member_key = get_contact_id_from_member(member_number)[0][0]
    except IndexError:
        return None

    #TODO: make this query work with company ID

    q = """
    SELECT 
       animal_table.id_animalid
       , flock_prefix_table.flock_prefix
       , animal_table.animal_name
       , animal_id_info_table.id_animalidinfoid
       , animal_id_info_table.id_number 
       , animal_id_info_table.official_id
       , id_type_table.id_idtypeid 
       , id_color_table.id_color_name
       , id_location_table.id_location_name
       , animal_table.birth_date
       , sex_table.sex_name
       , birth_type_table.birth_type
       , animal_registration_table.registration_number
    FROM
       (SELECT
           id_animalid
           , MAX(transfer_date)
           , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) AS owner
       FROM animal_ownership_history_table
       GROUP BY id_animalid) AS last_transfer_date
    LEFT JOIN animal_table ON animal_table.id_animalid = last_transfer_date.id_animalid
    LEFT JOIN animal_id_info_table ON animal_id_info_table.id_animalid = animal_table.id_animalid
    LEFT JOIN id_type_table ON id_type_table.id_idtypeid = animal_id_info_table.id_idtypeid
    LEFT JOIN id_color_table ON id_color_table.id_idcolorid = animal_id_info_table.id_male_id_idcolorid
    LEFT JOIN id_location_table ON id_location_table.id_idlocationid = animal_id_info_table.id_idlocationid
    LEFT JOIN birth_type_table ON animal_table.id_birthtypeid = birth_type_table.id_birthtypeid 
    LEFT JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
    LEFT JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
    LEFT JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
    LEFT JOIN animal_registration_table ON animal_table.id_animalid = animal_registration_table.id_animalid
    GROUP BY animal_registration_table.id_animalid
    HAVING SUM(CASE WHEN animal_registration_table.id_animalregistrationtypeid IN (2, 6, 7) THEN 1 ELSE 0 END) = 0
       AND SUM(CASE WHEN animal_registration_table.id_animalregistrationtypeid IN (1, 10, 11) THEN 1 ELSE 0 END) >= 1
       AND last_transfer_date.owner = ?
       AND animal_table.death_date = ''
    """

    ans = query_db(q, args=(member_key,))
    return ans


def get_birth_types():
    q = """
    SELECT
        birth_type
    FROM
        birth_type_table
    WHERE
        id_birthtypeid < 5
        OR
        id_birthtypeid > 41
    ORDER BY
        birth_type_display_order
    """
    return [x[0] for x in query_db(q)]


def get_breeder_name(animal_id):
    q = """
    SELECT
        id_breeder_id_contactid
        , MIN (registration_date)
        , IIF (id_breeder_id_contactid  = '' , id_breeder_id_companyid, id_breeder_id_contactid ) owner
    FROM animal_registration_table
    WHERE id_animalid = ?
		AND id_registry_id_companyid IN (25,54,55)
    """
    # Old Query
    ans = query_db(q, args=(animal_id,), one=True)
    # q = """
    # SELECT
    #     to_id_contactid
    #     , MIN(transfer_date)
    #     , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) owner
    # FROM animal_ownership_history_table
    # WHERE id_animalid = ?
    # """
    # ans = query_db(q, args=(animal_id,), one=True)

    q = """
    SELECT 
        id_contactid
        ,contact_table.contact_first_name || " " || contact_table.contact_last_name AS breeder
    FROM contact_table
    WHERE contact_table.id_contactid = ?
    """
    ans = query_db(q, args=(ans[2],), one=True)

    return ans[1]


def get_coat_colors():
    q = """
    SELECT 
    genetic_coat_color_table.coat_color 
    FROM genetic_coat_color_table 
    WHERE genetic_coat_color_table.id_registry_id_companyid IN (25, 54, 55)
    ORDER BY genetic_coat_color_table.coat_color_display_order;
    """
    return [x[0] for x in query_db(q)]


def get_coat_color_of_animal(animal_id):
    # print(f"DEBUG! -> {animal_id}")
    coat_q = """
            SELECT
    	        coat_color
            FROM
    	        genetic_coat_color_table
            WHERE
    	        id_geneticcoatcolorid = (SELECT
    								id_geneticcharacteristicvalueid
    							 FROM
    								animal_genetic_characteristic_table
    							 WHERE
    								id_animalid = ?);
            """
    try:
        color_tup = query_db(coat_q, args=(animal_id,), one=True)
        coat_color = color_tup[0]
    except TypeError:
        return "Unknown"
    return coat_color


def get_colors(with_id=False, with_default=False):
    if with_id:
        color_q = "SELECT id_idcolorid, id_color_name FROM id_color_table"
        colors = query_db(color_q)

        print("DEBUGGIN")
        print(colors)
        print(colors[0])

        if with_default:
            ret = [(-1, "Select Color")]
            ret.extend(colors)
            return ret
        return colors
    else:
        color_q = "SELECT id_color_name FROM id_color_table"
        colors = query_db(color_q)

        ret = [c[0] for c in colors]

        if with_default:
            defaulted_ret = ["Select Color"]
            defaulted_ret.extend(ret)
            return defaulted_ret
        return ret


def get_color(color_id):
    # stringify this in case a weird ID comes back and we cannot convert it to a number
    if str(color_id) == "-1":
        return "No Color"

    color_q = "SELECT id_color_name FROM id_color_table WHERE id_idcolorid = ?"
    colors = query_db(color_q, args=(color_id,), one=True)

    return colors[0]


def get_contact_info(current_user_id):
    q = f"""
    SELECT * FROM (
        SELECT
            owner_registration_table.id_ownerregistrationid
            , contact_table.id_contacttitleid
            , contact_table.contact_first_name
            , contact_table.contact_middle_name
            , contact_table.contact_last_name
            , contact_phone_table.contact_phone
            , contact_phone_table.id_phonetypeid
            , contact_email_table.contact_email
            , contact_email_table.id_emailtypeid
            , premise_table.id_premiseid
            , premise_table.premise_number
            , premise_table.premise_address1
            , premise_table.premise_address2
            , premise_table.premise_city
            , premise_table.premise_id_stateid
            , premise_table.premise_postcode
            , premise_table.premise_id_countyid
            , premise_table.premise_id_countryid
            FROM owner_registration_table
        INNER JOIN contact_table ON owner_registration_table.id_contactid = contact_table.id_contactid
        LEFT JOIN contact_phone_table ON contact_table.id_contactid = contact_phone_table.id_contactid
        LEFT JOIN contact_email_table ON contact_table.id_contactid = contact_email_table.id_contactid
        INNER JOIN contact_premise_table ON contact_table.id_contactid = contact_premise_table.id_contactid
        INNER JOIN premise_table ON contact_premise_table.id_premiseid = premise_table.id_premiseid
        WHERE
            membership_number = "{current_user_id}"
            AND contact_premise_table.end_premise_use = '' 
            AND owner_registration_table.id_registry_id_companyid = 25
        
        UNION
        
        SELECT
            owner_registration_table.id_ownerregistrationid
            , ''
            , ''
            , ''
            , company_table.company
            , company_phone_table.company_phone
            , company_phone_table.id_phonetypeid
            , company_email_table.company_email
            , company_email_table.id_emailtypeid
            , premise_table.id_premiseid
            , premise_table.premise_number
            , premise_table.premise_address1
            , premise_table.premise_address2
            , premise_table.premise_city
            , premise_table.premise_id_stateid
            , premise_table.premise_postcode
            , premise_table.premise_id_countyid
            , premise_table.premise_id_countryid
            FROM owner_registration_table
        INNER JOIN company_table ON owner_registration_table.id_companyid = company_table.id_companyid
        LEFT JOIN company_phone_table ON company_table.id_companyid = company_phone_table.id_companyid
        LEFT JOIN company_email_table ON company_table.id_companyid = company_email_table.id_companyid
        INNER JOIN company_premise_table ON company_table.id_companyid = company_premise_table.id_companyid
        INNER JOIN premise_table ON company_premise_table.id_premiseid = premise_table.id_premiseid
        WHERE
            membership_number = "{current_user_id}"
            AND company_premise_table.end_premise_use = ''
            AND owner_registration_table.id_registry_id_companyid = 25
    ) AS combined_members_list
	ORDER BY combined_members_list.contact_last_name
    """

    return query_db(q)


def get_conception_types():
    q = "SELECT id_servicetypeid, service_type FROM service_type_table;"
    conceptions = query_db(q)
    return conceptions


def get_conception_type(ct_id):
    q = "select service_type from service_type_table where id_servicetypeid = ?"
    ans = query_db(q, (ct_id,), one=True)[0]
    return ans


def get_contact_id_from_member(mem_num):
    q = """
    SELECT
          contact_table.id_contactid
         
            FROM owner_registration_table
        INNER JOIN contact_table ON owner_registration_table.id_contactid = contact_table.id_contactid
       
        WHERE
            membership_number = ?
    """
    return query_db(q, args=(mem_num,))


def get_company_id_from_member(mem_num):
    q = """
    SELECT
          company_table.id_companyid

            FROM owner_registration_table
        INNER JOIN company_table ON owner_registration_table.id_companyid = company_table.id_companyid

        WHERE
            membership_number = ?
    """
    return query_db(q, args=(mem_num,))

def get_current_members():
    q = """
    SELECT * 
	FROM (
        SELECT
           owner_registration_table.id_ownerregistrationid
           , owner_registration_table.membership_number
           , flock_prefix_table.flock_prefix
           , contact_table.id_contactid
           , ''
           , contact_table.contact_first_name
           , contact_table.contact_last_name
           , owner_registration_table.dues_paid_until
           , registry_membership_region_table.membership_region
           --, premise_table.premise_state
    FROM owner_registration_table
       INNER JOIN contact_table ON owner_registration_table.id_contactid = contact_table.id_contactid
       INNER JOIN registry_membership_region_table
    ON owner_registration_table.id_registrymembershipregionid = registry_membership_region_table.id_registrymembershipregionid
       INNER JOIN flock_prefix_table ON owner_registration_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
       WHERE
               owner_registration_table.id_registry_id_companyid IN (25,54,55)
               AND owner_registration_table.id_registrymembershipstatusid = 2
               AND owner_registration_table.id_registrymembershiptypeid IN (1, 3, 4)
    
       UNION
    
       SELECT
            owner_registration_table.id_ownerregistrationid
            , owner_registration_table.membership_number
            , flock_prefix_table.flock_prefix
            , ''
            , company_table.id_companyid
            , company_table.company
            , ''
            , owner_registration_table.dues_paid_until
            , registry_membership_region_table.membership_region
          --, premise_table.premise_state
    FROM owner_registration_table
        INNER JOIN company_table ON owner_registration_table.id_companyid = company_table.id_companyid
        INNER JOIN registry_membership_region_table ON owner_registration_table.id_registrymembershipregionid = registry_membership_region_table.id_registrymembershipregionid
        INNER JOIN flock_prefix_table ON owner_registration_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
        WHERE
           owner_registration_table.id_registry_id_companyid  IN (25,54,55)
           AND owner_registration_table.id_registrymembershipstatusid = 2
           AND owner_registration_table.id_registrymembershiptypeid IN (1, 3, 4)
   ) AS member_list
   ORDER BY Contact_last_name
   
    """
    return query_db(q)


def get_locations(with_default=False, official_only=False, farm_only=False):

    #TODO-newversion
    # when adding new species/registries, you will need to update what locations are returned

    if official_only:
        if with_default:
            return ["Select Location", "Left Ear", "Right Ear"]
        return ["Left Ear", "Right Ear"]

    if farm_only:
        if with_default:
            return ["Select Location", "Left Ear", "Right Ear", "Left Flank", "Right Flank", "Side", "Tail Web", "Unknown"]
        return ["Left Ear", "Right Ear", "Left Flank", "Right Flank", "Side", "Tail Web", "Unknown"]

    loc_q = "SELECT id_location_name FROM id_location_table"
    locations = query_db(loc_q)

    # return with default value for tabs
    if with_default:
        ans = ["Select Location"]
        ans.extend([loc[0] for loc in locations])
        return ans
    # otherwise return normally
    return [loc[0] for loc in locations]


def get_name_from_membership_number(current_user_id):
    q = f"""
       SELECT
           owner_registration_table.id_ownerregistrationid
      , contact_table.id_contactid
      , ''
           , contact_table.contact_first_name
           , contact_table.contact_last_name
           , owner_registration_table.dues_paid_until
     FROM owner_registration_table
       INNER JOIN contact_table ON owner_registration_table.id_contactid = contact_table.id_contactid
       WHERE
               membership_number = ?
               AND owner_registration_table.id_registry_id_companyid = 25
    
       UNION
    
       SELECT
           owner_registration_table.id_ownerregistrationid
      , ''
      , company_table.id_companyid
      , company_table.company
           , ''
           , owner_registration_table.dues_paid_until
     FROM owner_registration_table
       INNER JOIN company_table ON owner_registration_table.id_companyid = company_table.id_companyid
       WHERE
           membership_number = ?
           AND owner_registration_table.id_registry_id_companyid = 25
           """
    ans = query_db(q, args=(current_user_id, current_user_id), one=True)
    return ans


def get_offspring_data_of(animal_id):
    """
            <th>Registration Number</th>    GOT
            <th>Flock Prefix</th>           GOT
            <th>Animal Name</th>            GOT
            <th>Sex</th>                    GOT
            <th>Status</th>                 KINDA GOT
            <th>Birthdate</th>              GOT
            <th>Sire Name</th>
            <th>Dam Name</th>
            <th>Coat Color</th>
            <th>Breeder's Name</th>
            <th>Owner's Name</th>
    """

    q = "SELECT id_animalid, animal_name, birth_date, death_date, id_sexid FROM animal_table WHERE sire_id = ? OR dam_id = ?"
    ans = query_db(q, args=(animal_id, animal_id))
# this is the sex key for species sheep but will change for other species
    male = 1
    female = 2

    data = list()

    for row in ans:
        child_id = row[0]
        animal_name = row[1]
        birth_date = row[2]
        death_date = row[3]
        sex_id = row[4]
# todo this should pull out of the sex table not be hard coded so it's adaptable for other species
        sex = "Ram" if sex_id == male else "Ewe"  # get sex of animal

        reg_num = get_recent_reg_num(child_id)

        flock_q = "SELECT flock_prefix FROM flock_prefix_table WHERE id_flockprefixid = (SELECT id_flockprefixid FROM animal_flock_prefix_table WHERE id_animalid = ?);"
        try:
            flock_prefix = query_db(flock_q, args=(child_id,), one=True)[0]
        except TypeError:
            flock_prefix = ""

        sire_id, dam_id = get_parental_data_of(child_id)

        sire_name = get_single_animal_data(sire_id)
        dam_name = get_single_animal_data(dam_id)

        coat_color = get_coat_color_of_animal(child_id)

        breeder_name =get_breeder_name(child_id)
        owner_name = get_owner_name(child_id)

        row_data = (
            child_id, animal_name, birth_date, death_date, sex, reg_num, flock_prefix, sire_name, dam_name, coat_color,
            breeder_name, owner_name)
        data.append(row_data)
        # get table data for pedigree
    return data


def get_owner_name(animal_id):
    # first step: get contact ID (or company when applicable)
    q = """
    SELECT
                   to_id_contactid
                   , MAX(transfer_date)
                   , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) owner
               FROM animal_ownership_history_table
               WHERE id_animalid = ?
    """
    ans = query_db(q, args=(animal_id,), one=True)

    # step 2: when parsing data, verify if the contact Id is from a person or company
    #         NOTE: `id_to_search_by` can be a contact_id OR a company_id
    is_person = True
    try:
        if ans[0] != "":
            id_to_search_by = ans[0]
        elif ans[2] != "":
            id_to_search_by = ans[2]
            is_person = False
        else:
            return None

    except IndexError:
        return None

    # only do a name search if the contact is from a person, not a company as there can be overlap
    if is_person:
        q2 = """
        SELECT 
            id_contactid
            ,contact_table.contact_first_name || " " || contact_table.contact_last_name AS breeder
        FROM contact_table
        WHERE contact_table.id_contactid = ?
        """
        result = query_db(q2, args=(id_to_search_by,), one=True)

        if result is not None:
            return result[1]

    # search for company name from company_id
    q3 = """
    SELECT 
        id_companyid
        ,company_table.company AS breeder
    FROM company_table
    WHERE company_table.id_companyid = ?
    """
    result = query_db(q3, args=(id_to_search_by,), one=True)

    # try to parse data, otherwise return 'Unknown' in fail cases
    try:
        return result[1]
    except TypeError:
        return "Unknown"


def get_parental_data_of(animal_id):
    q1 = "SELECT sire_id, dam_id FROM animal_table WHERE id_animalid = ?"
    ans = query_db(q1, args=(animal_id,))
    return ans[0]  # this returns the tuple inside


def get_prefix(mem_id):

    prefix_q = """
    SELECT
    flock_prefix_table.id_flockprefixid
    , flock_prefix
    from flock_prefix_table
    INNER JOIN owner_registration_table ON flock_prefix_table.id_flockprefixid = owner_registration_table.id_flockprefixid

    where membership_number = ? LIMIT 1;
    """
    prefix = query_db(prefix_q, args=(mem_id,), one=True)
    return prefix[1]


def get_prefix_key(prefix):
    q = """
    Select id_flockprefixid from flock_prefix_table where flock_prefix = ?
    """
    ans = query_db(q, args=(prefix,), one=True)
    if type(ans) is tuple:
        return ans[0]
    return ans


def get_premise_id_by_contact(contact_id):
    q = """
    SELECT
	contact_table.id_contactid
	, contact_premise_table.id_premiseid
    FROM contact_table

    INNER JOIN contact_premise_table on contact_table.id_contactid = contact_premise_table.id_contactid
    INNER JOIN premise_table on contact_premise_table.id_premiseid = premise_table.id_premiseid

    WHERE contact_table.id_contactid =  ?
    AND contact_premise_table.end_premise_use = ''
	AND premise_table.id_premisetypeid IN (1,3)
    """
    ans = query_db(q, args=(contact_id,), one=True)
    if ans is not None and len(ans) >=2:
        return ans[1]
    return None

def get_premise_id_by_company(company_id):
    q = """
    SELECT
	company_table.id_companyid
	, company_premise_table.id_premiseid
    FROM company_table

    INNER JOIN company_premise_table on company_table.id_companyid = company_premise_table.id_companyid
    INNER JOIN premise_table on company_premise_table.id_premiseid = premise_table.id_premiseid

    WHERE company_table.id_companyid =  ?
    AND company_premise_table.end_premise_use = ''
	AND premise_table.id_premisetypeid IN (1,3)
    """
    ans = query_db(q, args=(company_id,), one=True)
    if ans is not None and len(ans) >= 2:
        return ans[1]
    return None

def get_coat_color_key(coat_color):
    q = """
        SELECT
        id_geneticcoatcolorid
        from genetic_coat_color_table
        WHERE  id_registry_id_companyid in (25, 54, 55) AND coat_color = ?;
        """
    ans = query_db(q, args=(coat_color,), one=True)
    if type(ans) is tuple:
        return ans[0]
    return ans


def get_tag_loc_key(loc):
    q = """
    SELECT
    id_idlocationid
    from id_location_table
    WHERE  id_location_name = ?
    """

    ans = query_db(q, args=(loc,), one=True)
    if type(ans) is tuple:
        return ans[0]

    return ans


def get_tag_type_name(tag_id):

    if str(tag_id) == '-1':
        return "No Tag Selected"

    q = "SELECT id_type_name FROM id_type_table WHERE id_idtypeid = ?"
    ans = query_db(q, args=(tag_id,), one=True)
    return ans[0]


def get_tag_color_key(tag_color):
    q = """
    SELECT
    id_idcolorid
    FROM id_color_table
    WHERE  id_color_name = ?
    """
    ans = query_db(q, args=(tag_color,), one=True)
    if type(ans) is tuple:
        return ans[0]
    return ans


def get_recent_reg_num(animal_id):
    # for getting reg num get the newest registration number
    reg_num_q = "SELECT registration_number, registration_date FROM animal_registration_table WHERE " \
                "id_animalid = ? AND id_registry_id_companyid IN (25, 54, 55)"

    ans = query_db(reg_num_q, args=(animal_id,))

    if len(ans) == 0:
        reg_num = "Not Registered"
    elif len(ans) == 1:
        # if only 1 registration number, then just get it
        reg_num = ans[0][0]
    else:
        # if multiple registration numbers, get the most recent one
        for_reg_num = list()

        # max_date =
        try:
            for reg, date_applied in ans:
                as_date = datetime.strptime(date_applied, "%Y-%m-%d")
                for_reg_num.append((reg, as_date))

            reg_num = max(for_reg_num, key=lambda x: x[1])[0]
        except ValueError:
            reg_num = "Error Encountered (Likely a format issue, let registrar know)"

    return reg_num


def get_single_animal_data(animal_id):

    q = "SELECT animal_name FROM animal_table WHERE id_animalid = ?"
    ans = query_db(q, args=(animal_id,), one=True)
    if ans is None:
        return None
    return ans[0]


def get_sires_and_dams_by_owner(company_id=None, contact_id=None):
    q = """
    SELECT animal_table.id_animalid
        , animal_registration_table.registration_number
        , flock_prefix_table.flock_prefix
        , animal_table.animal_name
        , animal_id_info_table.id_number 
        , sex_table.sex_abbrev
    FROM
        (SELECT
            id_animalid
            , MAX(transfer_date)
            , IIF (to_id_contactid  = '' , to_id_companyid, to_id_contactid ) owner
        FROM animal_ownership_history_table
        GROUP BY
            id_animalid
        ) AS last_transfer_date
 
    INNER JOIN animal_table ON animal_table.id_animalid = last_transfer_date.id_animalid
    INNER JOIN animal_id_info_table ON animal_id_info_table.id_animalid = animal_table.id_animalid
    INNER JOIN id_type_table ON id_type_table.id_idtypeid = animal_id_info_table.id_idtypeid
    INNER JOIN animal_flock_prefix_table ON animal_table.id_animalid = animal_flock_prefix_table.id_animalid
    INNER JOIN sex_table ON animal_table.id_sexid = sex_table.id_sexid
    INNER JOIN flock_prefix_table ON animal_flock_prefix_table.id_flockprefixid = flock_prefix_table.id_flockprefixid
    INNER JOIN
        (SELECT animal_registration_table.id_animalid
            , MAX(animal_registration_table.registration_date) AS registration_date
        FROM animal_registration_table
        WHERE
            animal_registration_table.id_animalregistrationtypeid IN (2 ,6 ,7 ) --registered Black, Cocolate,White Welsh
            AND animal_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
        GROUP BY animal_registration_table.id_animalid
        ) AS current_registration_date ON animal_table.id_animalid = current_registration_date.id_animalid
    INNER JOIN animal_registration_table ON animal_table.id_animalid = animal_registration_table.id_animalid AND animal_registration_table.registration_date = current_registration_date.registration_date
    WHERE 
        animal_id_info_table.id_date_off = '' 
        AND animal_id_info_table.official_id = 1
        AND animal_table.death_date = "" 
        AND  last_transfer_date.owner = ?  -- This is what you will need to pass the query.
    ORDER BY 
        sex_table.sex_abbrev ASC
        , animal_registration_table.registration_number
    """

    # FIXME implement this
    # WEWLAD this needs to be checked with oogie

    id_to_check = contact_or_company_id(contact_id, company_id)


def get_sexes():
    q = """
    SELECT
        sex_name
    FROM
        sex_table
    WHERE
        id_speciesid = 1
    """
    return [x[0] for x in query_db(q)]


def get_states():
    q = "SELECT id_stateid, state_name FROM state_table;"
    return query_db(q)


def get_state_key(state):
    q = "SELECT id_stateid FROM state_table where state_name = ?;"
    result = query_db(q, (state,), one=True)
    return result[0] if result else None


def get_id_info(animal_id):
    q = """
    SELECT 
		animal_id_info_table.id_animalidinfoid AS _id
        , animal_table.animal_name
        , animal_id_info_table.id_number
        , id_location_table.id_location_name
        , id_color_table.id_color_name
        , id_type_table.id_type_name
        , animal_id_info_table.official_id
  --      , animal_id_info_table.id_date_off  
    FROM animal_table 
    INNER JOIN animal_id_info_table ON animal_table.id_animalid = animal_id_info_table.id_animalid 
    LEFT OUTER JOIN id_color_table ON animal_id_info_table.id_male_id_idcolorid = id_color_table.id_idcolorid 
    LEFT OUTER JOIN id_location_table ON animal_id_info_table.id_idlocationid = id_location_table.id_idlocationid 
    INNER JOIN id_type_table ON animal_id_info_table.id_idtypeid = id_type_table.id_idtypeid 
    WHERE animal_id_info_table.id_animalid = ?
		AND (animal_id_info_table.id_date_off  IS NULL OR  animal_id_info_table.id_date_off   = '')
    ORDER BY id_type_name ASC;
    """

    result = query_db(q, args=(animal_id,))
    return result


def get_id_remove_reasons():
    reason_q = "SELECT id_remove_reason FROM id_remove_reason_table"
    reasons = query_db(reason_q)
    return [reason[0] for reason in reasons]


def get_types(with_id=False, with_default=False):
    if not with_id:
        type_q = "SELECT id_type_name FROM id_type_table WHERE id_idtypeid IN (1 ,2 ,4 ,5 ,9)"
        types = query_db(type_q)

        if with_default:
            ans = ["Select Type"]
            ans.extend([ty[0] for ty in types])
            return ans

        return [ty[0] for ty in types]
    else:
        type_q = "SELECT id_idtypeid, id_type_name FROM id_type_table WHERE id_idtypeid IN (1 ,2 ,4 ,5 ,9)"
        types = query_db(type_q)

        if with_default:
            ans = [(-1, "Select Type")]
            ans.extend([ty for ty in types])
            return ans

        return [ty for ty in types]


def get_sex_key(sex):
    q = """
        SELECT id_sexid FROM sex_table WHERE sex_standard = ? AND id_speciesid = 1
        """

    result = query_db(q, args=(sex,), one=True)
    if result is None:
        return None
    return result[0]


def get_weight_units_key(units):
    q = """
        SELECT id_unitsid FROM units_table WHERE units_abbrev = ?
        """

    result = query_db(q, args=(units,), one=True)
    if result is None:
        return None
    return result[0]


def is_official_tag(id_number):
    q = "SELECT official_tag FROM animal_id_info_table WHERE id_number = ?"

    ans = query_db(q, args=(id_number,), one=True)
    if ans is None:
        return False
    # TODO:

    return ans[0]


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


# def get_fed_color_key(fed_color):
#     print(f"FED COLOR: {fed_color}")
#     q = "SELECT id_idcolorid from id_color_table where id_color_name = ?"
#     ans = query_db(q, args=(fed_color,), one=True)
#     return ans[0]


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def verify_user(mem_num, password):
    # "username" is actually membership number
    # password is still password
    # Owner_registration_table -> username stuff

    query = f"SELECT EXISTS(SELECT * from owner_registration_table WHERE membership_number=\"{mem_num}\");"

    x = query_db(query, one=True)

    if x[0] == 1:  # user membership number exists

        # check if passwords match
        password_q = f"SELECT online_password FROM Owner_registration_table WHERE membership_number=\"{mem_num}\""
        y = query_db(password_q, one=True)

        # TODO: get query to get name from user
        name = "placeholder_name"

        user_password = y[0]

        # membership_number / password combo exists
        if password == user_password:

            user_query = f"""
                    SELECT
                     id_ownerregistrationid
                     , id_contactid
                     , id_companyid
                     -- , dues_paid_until
                     , id_registrymembershipregionid
                     , id_flockprefixid
                     , id_registrymembershipstatusid
                     , id_registrymembershiptypeid
                     -- , board_member
                     -- , last_census
                     -- , id_registryprivacyid

                    FROM owner_registration_table 
                    WHERE 
                     membership_number = "{mem_num}"
                     AND owner_registration_table.id_registry_id_companyid IN (25) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
                    """

            # this was changed to get only 1 row:
            #  AND owner_registration_table.id_registry_id_companyid IN ( 25 ,54 ,55) -- ABWMSA Black Welsh, ACWMSA Chocolate Welsh, AWWMSA White Welsh
            # RESULT with section above in query ^^:
            # [(1, 1, '', 3, 1, 2, 1), (587, 1, '', 3, 1, 2, 1)]

            result = query_db(user_query)

            # id_owner_registration_id = result[0]
            contact_id = result[0][1]
            company_id = result[0][2]

            return User(mem_num=mem_num, password=password, name=name, contact_id=contact_id, company_id=company_id)
        else:
            return None
    else:
        return None
