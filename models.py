
from flask_login import UserMixin
import database.database as db


USERS = dict()


def user_loader_helper(user_id, password=None):

    if user_id in USERS.keys():
        return USERS[user_id]
    elif password is not None:
        user = db.verify_user(user_id, password)
        USERS[user_id] = user
        return user
    else:
        return None


def add_user(user_id, user):
    USERS[user_id] = user


class User(UserMixin):

    # id_idx = 0

    def __init__(self, mem_num, password, name, contact_id, company_id):
        self.id = mem_num
        self.name = name
        # User.id_idx += 1
        self.membership_number = mem_num
        self.password = password
        self.contact_id = contact_id
        self.company_id = company_id

    # this proves when using "current_user" from flask in the view.py files that
    # we can call user based functions. It won't be recognized but it works!

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)  # id must be a string




