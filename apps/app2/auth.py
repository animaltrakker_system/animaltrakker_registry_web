from flask import Blueprint, redirect, url_for, render_template, flash, request

from flask_login import login_user, logout_user

from database.database import verify_user

from models import add_user

#from . import db

auth = Blueprint('auth', __name__, template_folder="templates/")


@auth.route('/login', methods=['GET', 'POST'])
def login():

    if request.method == 'POST':

        # login code goes here
        membership_number = request.form.get('email')
        password = request.form.get('password')
        remember = True if request.form.get('remember') else False

        # user = User.query.filter_by(email=email).first()

        user = verify_user(membership_number, password)

        if not user:
            flash('Please check your login details and try again.')
            return redirect(url_for('auth.login'))  # if the user doesn't exist or password is wrong, reload the page

        login_user(user)
        add_user(membership_number, user)  # add user to USERS in models.py

        # if the above check passes, then we know the user has the right credentials
        return redirect(url_for('app1.home'))  # FIXME change to home screen

    return render_template('/auth/login.html')


@auth.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_user()
    return redirect(url_for('auth.login'))


# @auth.route('/login', methods=['POST'])
# def login_post():
#
#     print("\n\n\n\n WEWEWEWEWEWEWEWE \n\n\n\n\n\n")
#
#     # login code goes here
#     email = request.form.get('email')
#     password = request.form.get('password')
#     remember = True if request.form.get('remember') else False
#
#     #user = User.query.filter_by(email=email).first()
#
#     user = verify_user(email, password)
#
#     # if not user or not check_password_hash(user.password, password):
#
#     # check if the user actually exists
#     # take the user-supplied password, hash it, and compare it to the hashed password in the database
#
#     # OLD CASE:
#     # if not user or not check_password_hash(user.password, password):
#     if not user:
#         flash('Please check your login details and try again.')
#         return redirect(url_for('auth.login'))  # if the user doesn't exist or password is wrong, reload the page
#
#     login_user(user)
#
#     # if the above check passes, then we know the user has the right credentials
#     return redirect(url_for('app1.index'))


@auth.route('/signup')
def signup():
    return render_template('signup.html')

@auth.route('/signup', methods=['POST'])
def signup_post():
    # code to validate and add user to database goes here
    return redirect(url_for('auth.login'))
