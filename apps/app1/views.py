from datetime import datetime
from flask import session, Blueprint, jsonify, request, url_for, redirect, render_template, flash

from flask_login import current_user, login_required

import database.database as db
import stage


from models import USERS  # FIXME: remove this after debugging

app1 = Blueprint('app1', __name__, template_folder="templates/")


@app1.route("/")
def base():
    return redirect(url_for('app1.home'))


@app1.app_errorhandler(404)
def page_not_found(e):
    return render_template('/error_pages/404.html'), 404


@app1.app_errorhandler(500)
def page_not_found(e):
    return render_template('/error_pages/500.html'), 500


@app1.route("/animalsearch/", methods=('GET', 'POST'))
@login_required
def animal_search():
    content = dict()

    content['coat_colors'] = db.get_coat_colors()
    content['coat_color_filter_value'] = ""

    content['sexes'] = db.get_sexes()

    if request.method == 'POST':

        # add parameters to query
        form_data = request.form.to_dict()

      #  print("DEBUG!!!!!!!")
      #  print(form_data)

        name = None
        reg_num = None
        flock = None
        coat_color = None


        if form_data['name-filter'] != '':
            name = form_data['name-filter']
            content['name_filter_value'] = name

        if form_data['regNum-filter'] != '':
            reg_num = form_data['regNum-filter']
            content['reg_num_filter_value'] = reg_num

        if form_data['flock-filter'] != '':
            flock = form_data['flock-filter']
            content['flock_filter_value'] = flock

        # if form_data['owner-filter'] != '':
        #     flock = form_data['owner-filter']
        #     content['owner_filter_value'] = owner

        if form_data['coat-filter'].lower() != "any":
            coat_color = form_data['coat-filter'].lower()
            content['coat_color_filter_value'] = coat_color

        sex_filter = form_data['sex-filter']
        content['sex_filter'] = sex_filter
        only_alive_animals = not "all_animals" in form_data.keys()

        # animals = db.get_all_animals(name=name, reg_num=reg_num, flock=flock, coat_color=coat_color, sex_filter=sex_filter, only_alive_animals=only_alive_animals, owner = owner_member_number)
        animals = db.get_all_animals(name=name, reg_num=reg_num, flock=flock, coat_color=coat_color,
                                     sex_filter=sex_filter, only_alive_animals=only_alive_animals)

        if len(animals) > 500:
            content['err_message'] = "More than 500 animals found, please refine your search."
            return render_template("/animalSearch/animal_search.html", **content)
        else:
            content['animals'] = animals

        return render_template("/animalSearch/animal_search.html", **content)

    return render_template("/animalSearch/animal_search.html", **content)


@app1.route("/births/", methods=('GET', 'POST'))
@login_required
def birth_details():

    if request.method == 'POST':

        content = request.form.to_dict()

        if content['dob'] == '':
            flash("Please enter a date of birth.")
            return redirect(url_for("app1.birth_details", **content))

        session['chosen_sire'] = content['sire']

        return redirect(url_for('app1.offspring', **content))

    content = request.args.to_dict()

    content['conceptionTypes'] = db.get_conception_types()

    now = datetime.now()
    content['date'] = now.strftime("%Y-%m-%d")  # get current date

    if current_user.contact_id != '':
        content['sires'] = db.all_alive_sires_owned_currently(contact_id=current_user.contact_id)
    elif current_user.company_id != '':
        content['sires'] = db.all_alive_sires_owned_currently(company_id=current_user.company_id)
    else:
        raise Exception("User has no contact or company ID")

    # get contact or company id to get dams
    if current_user.contact_id != '':
        content['dams'] = db.all_alive_dams_owned_currently(contact_id=current_user.contact_id)
    elif current_user.company_id != '':
        content['dams'] = db.all_alive_dams_owned_currently(company_id=current_user.company_id)
    else:
        raise Exception("User has no contact or company ID")

    try:
        chosen_sire = session['chosen_sire']
    except Exception as e:
        chosen_sire = ''

    content['chosen_sire'] = chosen_sire

    return render_template("/birthing/birthdetails.html", **content)


@app1.route("/changecontact/", methods=('GET', 'POST'))
@login_required
def change_contact_info():

    if request.method == 'POST':

        # define prefixes to pull data from form
        prefix_first_name = "contact_first_name_"
        prefix_middle_name = "contact_middle_name_"
        prefix_last_name = "contact_last_name_"
        prefix_phone = "phone_"
        prefix_email = "email_"
        prefix_premise_number = "premise_number_"
        prefix_address_1 = "address_1_"
        prefix_address_2 = "address_2_"
        prefix_city = "city_"
        prefix_state = "state_"
        prefix_postcode = "postcode_"

        form_data = request.form.to_dict()
        checked = [q for q in form_data.keys() if q.startswith("cb")]

        contact_content = list()

        for i, cb in enumerate(checked):
            row = dict()  # create list for single row
            row_num = cb.split("_")[1]  # get the row number
            # fill row data
            row["first_name"] = form_data[prefix_first_name + row_num]
            row["middle_name"] = form_data[prefix_middle_name + row_num]
            row["last_name"] = form_data[prefix_last_name + row_num]
            row["phone"] = form_data[prefix_phone + row_num]
            row["email"] = form_data[prefix_email + row_num]
            row["premise_number"] = form_data[prefix_premise_number + row_num]
            row["address_1"] = form_data[prefix_address_1 + row_num]
            row["address_2"] = form_data[prefix_address_2 + row_num]
            row["city"] = form_data[prefix_city + row_num]
            row["state"] = form_data[prefix_state + row_num]
            row["postcode"] = form_data[prefix_postcode + row_num]

            contact_content.append(row)

        session["contact_content"] = contact_content
        return redirect(url_for('app1.change_contact_info_verify'))

    session["user_info"] = db.get_contact_info(current_user.id)

    states = db.get_states()

    return render_template("/contactInfo/change_contact_info.html", user_info=session["user_info"], state_info=states)


@app1.route("/change_contact_verify/", methods=('GET', 'POST'))
@login_required
def change_contact_info_verify():
    states = db.get_states()

    if request.method == 'POST':
        form_data = request.form.to_dict()

        # get number of iterations to run
        idx = 1
        cont = True
        while cont:
            if f"old_contact_first_name_{idx}" not in form_data.keys():
                cont = False
            else:
                idx += 1

        # prefixes
        new = "new_"
        old = "old_"
        # prefixes!
        prefix_contact_first_name = "contact_first_name_"
        prefix_contact_middle_name = "contact_middle_name_"
        prefix_contact_last_name = "contact_last_name_"
        prefix_phone = "phone_"
        prefix_email = "email_"
        prefix_premise_number = "premise_number_"
        prefix_address_1 = "address_1_"
        prefix_address_2 = "address_2_"
        prefix_city = "city_"
        prefix_state = "state_"
        prefix_postcode = "postcode_"

        # prefixes
        data = list()
        # for i, c in checked:
        for i in range(1, idx):
            old_fn = form_data[old + prefix_contact_first_name + str(i)]
            old_mn = form_data[old + prefix_contact_middle_name + str(i)]
            old_ln = form_data[old + prefix_contact_last_name + str(i)]
            old_phone = form_data[old + prefix_phone + str(i)]
            old_email = form_data[old + prefix_email + str(i)]
            old_premise_number = form_data[old + prefix_premise_number + str(i)]
            old_address_1 = form_data[old + prefix_address_1 + str(i)]
            old_address_2 = form_data[old + prefix_address_2 + str(i)]
            old_city = form_data[old + prefix_city + str(i)]
            old_state = form_data[old + prefix_state + str(i)]
            old_postcode = form_data[old + prefix_postcode + str(i)]

            new_fn = form_data[new + prefix_contact_first_name + str(i)]
            new_mn = form_data[new + prefix_contact_middle_name + str(i)]
            new_ln = form_data[new + prefix_contact_last_name + str(i)]
            new_phone = form_data[new + prefix_phone + str(i)]
            new_email = form_data[new + prefix_email + str(i)]
            new_premise_number = form_data[new + prefix_premise_number + str(i)]
            new_address_1 = form_data[new + prefix_address_1 + str(i)]
            new_address_2 = form_data[new + prefix_address_2 + str(i)]
            new_city = form_data[new + prefix_city + str(i)]
            new_state = form_data[new + prefix_state + str(i)]
            new_postcode = form_data[new + prefix_postcode + str(i)]

            row = [old_fn, old_mn, old_ln, old_phone, old_email, old_premise_number, old_address_1, old_address_2, old_city,
                   old_state, old_postcode, new_fn, new_mn, new_ln, new_phone, new_email, new_premise_number, new_address_1, new_address_2,
                   new_city, new_state, new_postcode]

            data.append(row)

        result = stage.stage_contact_updates(current_user, data)

        if result:
            # good!
            flash("Data successfully submitted. Waiting for registrar approval. "
                  "No further action from you is required.")
            # del session["contact_content"]  # delete key to save space in user cookies
            return redirect(url_for("app1.home"))
        else:
            # bad!
            flash("There was an error processing your request. Please contact and administrator.")
            return redirect(url_for('app1.change_contact_info_verify'))

    return render_template("/contactInfo/contact_info_verify.html", user_info=session['contact_content'], state_info=states)


@app1.route("/deaths/", methods=('GET', 'POST'))
@login_required
def deaths():
    if request.method == 'POST':
        # define prefixes to pull data from form
        prefix_dod = "dod_"
        prefix_name = "name_"
        prefix_regnum = "regnum_"
        prefix_reason = "reason_"
        prefix_notes = "notes_"
        prefix_prefix = "prefix_"
        prefix_animal_id = "animal_id_"

        form_data = request.form.to_dict()
        checked = [q for q in form_data.keys() if q.startswith("cb")]

        death_content = list()

        for i, cb in enumerate(checked):
            row = dict()  # create list for single row
            row_num = cb.split("_")[1]  # get the row number
            # fill row data
            row["animal_id"] = form_data[prefix_animal_id + row_num]
            row["dod"] = form_data[prefix_dod + row_num]
            row["name"] = form_data[prefix_name + row_num]
            row["regnum"] = form_data[prefix_regnum + row_num]
            row["reason"] = form_data[prefix_reason + row_num]
            row["notes"] = form_data[prefix_notes + row_num]
            row["prefix"] = form_data[prefix_prefix + row_num]

            death_content.append(row)
        session["death_content"] = death_content

        flash("")  # looks like it does nothing but it actually prevents some weird "Welcome! message on death verify"
        return redirect(url_for('app1.death_verify'))

    content = dict()

    content['prefix'] = db.get_prefix(current_user.id)
    content['death_reasons'] = db.death_reasons()

    if current_user.contact_id != '':
        content['animal_info'] = db.get_animal_info_by_owner(contact_id=current_user.contact_id, get_coat_colors=True)
    elif current_user.company_id != '':
        content['animal_info'] = db.get_animal_info_by_owner(contact_id=current_user.company_id, get_coat_colors=True)
    else:
        raise Exception("Current user has no contact or company id")
    now = datetime.now()
    content['date'] = now.strftime("%Y-%m-%d")

    return render_template("/deaths/death.html", **content)


@app1.route("/death_verify/", methods=('GET', 'POST'))
@login_required
def death_verify():

    if request.method == 'POST':

        # define prefixes to pull data from form
        prefix_dod = "dod_"
        prefix_name = "name_"
        prefix_regnum = "regnum_"
        prefix_reason = "reason_"
        prefix_notes = "notes_"
        prefix_prefix = "prefix_"
        prefix_animal_id = "animal_id_"

        form_data = request.form.to_dict()

        death_staging = list()
        cont = True
        i = 1
        while cont:
            if f"dod_{i}" in form_data.keys():
                row = dict()  # create list for single row
                row_num = str(i)
                # fill row data
                row["animal_id"] = form_data[prefix_animal_id + row_num]
                row["dod"] = form_data[prefix_dod + row_num]
                row["name"] = form_data[prefix_name + row_num]
                row["regnum"] = form_data[prefix_regnum + row_num]
                row["reason"] = form_data[prefix_reason + row_num]
                row["notes"] = form_data[prefix_notes + row_num]
                row["prefix"] = form_data[prefix_prefix + row_num]

                if row['reason'] == "--Select Reason--":
                    flash("You must specify a death reason")
                    return redirect(url_for('app1.death_verify'))

                death_staging.append(row)

                i += 1
            else:
                cont = False

            #death_staging.append(row)

        session['death_staging'] = death_staging  # TODO: get data from page

        return redirect(url_for('app1.stage_deaths'))

    staging_content = dict()
    staging_content['death_reasons'] = db.death_reasons_no_stillborn()
    staging_content['deaths_for_staging'] = session['death_content']

    return render_template("/deaths/death_staging.html", **staging_content)


@app1.route("/home/", methods=('GET', 'POST'))
@login_required
def home():
    content = dict()
    # content['name'] = current_user.name  # get name of current user

    # TODO: check if dues are good

    wew = db.get_name_from_membership_number(current_user.id)
    first_name = wew[3]
    last_name = wew[4]
    name = f"{first_name} {last_name}"
    content['name'] = name

    # check dues
    datetime_str = wew[5]
    dues_paid_until = datetime.strptime(datetime_str, '%Y-%m-%d')
    # dues_paid_until = datetime.strptime('2023-05-22', '%Y-%m-%d')

    if dues_paid_until < datetime.now():
        flash('Dues not paid! Please go to www.blackwelsh.org/store and purchase a membership.')

    session['name'] = name
    return render_template("/home.html", **content)


@app1.route("/id_updates/", methods=('GET', 'POST'))
@login_required
def id_updates():
    content = dict()

    # add data to content
    # content['colors'] = db.get_colors()
    # content['locations'] = db.get_locations()
    # content['reasons'] = db.get_id_remove_reasons()
    # content['types'] = db.get_types()
    # content["id_info"] = db.get_animal_id_info()

    # id_animalid, fedid, fedidlocation, fedidcolor, farmid, farmidlocation
    # farmidcolor, flock_prefix, animal_name, birth_date, sex_abbrev, birth_type
    # TODO sort this list?
    # content["id_info"] = db.get_animal_id_info()

    # db.get_id_info("10534")

    if request.method == 'POST':
        # get key templates to get from request form
        prefix_prefix = "prefix_"
        prefix_name = "name_"
        prefix_tagnum = "tagnum_"
        prefix_type = "type_"
        prefix_color = "color_"
        prefix_location = "location_"
        prefix_date_off = "date_off_"
        prefix_sex = "sex_"
        prefix_regnum = "regnum_"
        prefix_official = "is_official_cb_"

        # get data from form
        form_data = request.form.to_dict()
        # get all checked form pieces
        checked = [q for q in form_data.keys() if q.startswith("cb")]

        rows = list()  # create list for storing row information to send to next page

        for i, cb in enumerate(checked):
            row = dict()  # create list for single row
            row_num = cb.split("_")[1]  # get the row number
            # fill row data
            row["name"] = form_data[prefix_name + row_num]
            row["prefix"] = form_data[prefix_prefix + row_num]
            row["tagnum"] = form_data[prefix_tagnum + row_num]
            row["type"] = form_data[prefix_type + row_num]
            row["color"] = form_data[prefix_color + row_num]
            row["loc"] = form_data[prefix_location + row_num]
            row["date_off"] = form_data[prefix_date_off + row_num]
            row["sex"] = form_data[prefix_sex + row_num]
            row["regnum"] = form_data[prefix_regnum + row_num]

            if prefix_official + row_num in form_data.keys():
                row['official'] = True
            else:
                row['official'] = False

            # row['official'] = form_data[prefix_official + row_num]
            print(row)
            rows.append(row)  # add row to list of rows
            # content[f'animal_{i}'] = row

        session['id_update_animals'] = rows

        #content["rows"] = rows  # set content to be passed into render_template

        #return render_template("/IDUpdates/update_tags.html", **content)
        return redirect(url_for('app1.id_update_verify'))

    content['colors'] = db.get_colors()
    content['locations'] = db.get_locations()
    content['reasons'] = db.get_id_remove_reasons()

    types_dict = dict()

    for type_key, type_value in db.get_types(with_id=True):
        types_dict[type_key] = type_value

    # content['types'] = db.get_types(with_id=True)
    content['types'] = types_dict

    if current_user.contact_id != '':
        content["id_info"] = db.get_animal_id_info(contact_id=current_user.contact_id)
    elif current_user.company_id != '':
        content["id_info"] = db.get_animal_id_info(company_id=current_user.company_id)
    else:
        raise Exception("User has no contact or company id")

    return render_template("/IDUpdates/id_updates.html", **content)


@app1.route("/id_update_verify/", methods=('GET', 'POST'))
@login_required
def id_update_verify():
    # content = request.args.to_dict()

    # data for when user comes from the previous id_updates page
    staging_content = dict()
    staging_content['animals_for_staging'] = session['id_update_animals']
    staging_content['types'] = db.get_types()
    staging_content['colors'] = db.get_colors()
    staging_content['locations'] = db.get_locations()
    staging_content['reasons'] = db.get_id_remove_reasons()

    return render_template("/IDUpdates/update_tags.html", **staging_content)


@app1.route("/markforsale/", methods=('GET', 'POST'))
@login_required
def mark_for_sale():
    content = dict()

    if request.method == 'POST':
        form_data = request.form.to_dict()
        checked = [q for q in form_data.keys() if q.startswith("cb")]

        prefix_regnum = "regnum_"
        prefix_flock = "flock_"
        prefix_name = "name_"
        prefix_sex = "sex_"

        data = list()


        for i, c in enumerate(checked):

            # get the idx of the animal selected
            idx = c.split("_")[1]

            regnum = form_data[prefix_regnum + idx]
            flock = form_data[prefix_flock + idx]
            name = form_data[prefix_name + idx]
            sex = form_data[prefix_sex + idx]

            row = (regnum, flock, name, sex)
            data.append(row)

        # stage db
        result = stage.stage_animal_for_sale(current_user, data)

        if result:
            # good!
            flash("Data successfully submitted. Waiting for registrar approval. "
                  "No further action from you is required.")
            return redirect(url_for("app1.home"))
        else:
            # bad!
            flash("There was an error processing your request. Please contact and administrator.")
            return redirect(url_for('app1.mark_for_stud', _method='GET'))

    #################################################################################
    #TODO This is the one I need to change to sort by birthdate and make a custom query for here
    if current_user.contact_id != '':
        all_animals = db.all_alive_registered_animals_owned_by(contact_id=current_user.contact_id)
    elif current_user.company_id != '':
        all_animals = db.all_alive_registered_animals_owned_by(company_id=current_user.company_id)
    else:
        raise Exception("User has no Company ID or Contact ID")

    content['animals'] = all_animals

    return render_template("/markForSale/mark_for_sale.html", **content)


@app1.route("/markforstud/", methods=('GET', 'POST'))
@login_required
def mark_for_stud():
    content = dict()

    if request.method == 'POST':
        form_data = request.form.to_dict()
        checked = [q for q in form_data.keys() if q.startswith("cb")]

        prefix_regnum = "regnum_"
        prefix_flock = "flock_"
        prefix_name = "name_"


        data = list()

        for i, c in enumerate(checked):
            regnum = form_data[prefix_regnum + str(i + 1)]
            flock = form_data[prefix_flock + str(i + 1)]
            name = form_data[prefix_name + str(i + 1)]

            row = (regnum, flock, name)
            data.append(row)

        # stage db
        result = stage.stage_animal_for_stud(current_user, data)

        if result:
            # good!
            flash("Data successfully submitted. Waiting for registrar approval. "
                  "No further action from you is required.")
            return redirect(url_for("app1.home"))
        else:
            # bad!
            flash("There was an error processing your request. Please contact and administrator.")
            return redirect(url_for('app1.mark_for_stud', _method='GET'))

    #################################################################################

    if current_user.contact_id != '':
        all_animals = db.all_alive_sires_owned_currently(contact_id=current_user.contact_id)
    elif current_user.company_id != '':
        all_animals = db.all_alive_sires_owned_currently(company_id=current_user.company_id)
    else:
        raise Exception("User has no Company ID or Contact ID")

    # studs = [x for x in all_animals if x[5].lower() == 'ram']
    content['animals'] = all_animals

    return render_template("/markForStud/mark_for_stud.html", **content)


@app1.route("/membersearch/", methods=('GET', 'POST'))
@login_required
def member_search():
    content = dict()
    # TODO: make this sorting SQL based and not python based
   # content['members'] = sorted(db.get_current_members(), key=lambda x: x[5])
    content['members'] = db.get_current_members()
    return render_template("/memberSearch/memberSearch.html", **content)


@app1.route("/my_flock/", methods=('GET', 'POST'))
@login_required
def my_flock():
    if request.method == 'POST':
        # define prefixes to pull data from form

        prefix_name = "name_"
        prefix_regnum = "regnum_"
        prefix_prefix = "prefix_"
        prefix_animal_id = "animal_id_"
        prefix_fed_id_num = "fed_id_num_"
        prefix_farm_tag = "farm_tag_"
        prefix_sex = "sex_"

        form_data = request.form.to_dict()
        checked = [q for q in form_data.keys() if q.startswith("cb")]

    #    death_content = list()

        for i, cb in enumerate(checked):
            row = dict()  # create list for single row
            row_num = cb.split("_")[1]  # get the row number
            # fill row data
            row["animal_id"] = form_data[prefix_animal_id + row_num]
            row["name"] = form_data[prefix_name + row_num]
            row["regnum"] = form_data[prefix_regnum + row_num]
            row["prefix"] = form_data[prefix_prefix + row_num]
            row["fed_id_num"] = form_data[prefix_fed_id_num + row_num]
            row["farm_tag_"] = form_data[prefix_farm_tag + row_num]
            row["sex"] = form_data[prefix_sex + row_num]
         #   death_content.append(row)
        #session["death_content"] = death_content

       #  flash("")  # looks like it does nothing but it actually prevents some weird "Welcome! message on death verify"
       #  return redirect(url_for('app1.death_verify'))

    content = dict()

    content['prefix'] = db.get_prefix(current_user.id)

    if current_user.contact_id != '':
        content['animal_info'] = db.get_animal_info_by_owner(contact_id=current_user.contact_id, get_coat_colors=True)
    elif current_user.company_id != '':
        content['animal_info'] = db.get_animal_info_by_owner(contact_id=current_user.company_id, get_coat_colors=True)
    else:
        raise Exception("Current user has no contact or company id")
    now = datetime.now()
    content['date'] = now.strftime("%Y-%m-%d")

    return render_template("/myflock/myflock.html", **content)
    #content['coat_colors'] = db.get_coat_colors()
    #content['coat_color_filter_value'] = ""

    #content['sexes'] = db.get_sexes()

    if request.method == 'POST':
        form_data = request.form.to_dict()


@app1.route("/offspring/", methods=('GET', 'POST'))
@login_required
def offspring():

    if request.method == 'POST':

        # TODO: oogie
        # when they submit their data, you will want to check that the required fields are filled out
        #

        form_data = request.form.to_dict()

        form_data['sire_id'] = session['sire_birth_id']
        form_data['dam_id'] = session['dam_birth_id']
        form_data['birth_notes'] = session['birth_notes']
        form_data['conception_type_id'] = session['conception_type_id']

        num_born = int(form_data['numberBorn'])
        # parse text from
        for i in range(0, num_born):
            # get color DB KEY and name
            form_data[f'ft_color_{i}'] = form_data[f'ft_color_{i}'][3:]
            form_data[f'ft_color_name_{i}'] = db.get_color(form_data[f'ft_color_{i}'])

            form_data[f'ft_type_{i}'] = form_data[f'ft_type_{i}']
            form_data[f'ft_type_name_{i}'] = db.get_tag_type_name(form_data[f'ft_type_{i}'])

            form_data[f'color_{i}'] = form_data[f'color_{i}']
            form_data[f'color_name_{i}'] = db.get_color(form_data[f'color_{i}'])

            form_data[f'type_{i}'] = form_data[f'type_{i}']
            form_data[f'type_name_{i}'] = db.get_tag_type_name(form_data[f'type_{i}'])

        result = stage.staging_offspring_file_creation(current_user, form_data)

        if result:
            # good!
            flash(
                "Data successfully submitted. Waiting for registrar approval. No further action from you is required.")
            return redirect(url_for("app1.home"))
        else:
            # get old data user input so they don't have to reenter it
            content = dict()
            content['numberBorn'] = int(form_data['numberBorn'])
            content['dob'] = form_data['dob']
            content['dam'] = form_data['dam']
            content['sire'] = form_data['sire']
            content['conceptionType'] = form_data['conceptionType']

            content['colors'] = db.get_colors(with_id=True, with_default=True)
            content['official_locations'] = db.get_locations(with_default=True,official_only=True)
            content['farm_locations'] = db.get_locations(with_default=True, farm_only=True)
            content['types'] = db.get_types(with_id=True, with_default=True)

            content['prefix'] = db.get_prefix(current_user.id)
            content['coat_colors'] = db.get_coat_colors()  # get coat colors

            flash("Submission Failed. Please verify that all data is present")
            # render same template again
            return render_template("/birthing/offspringdetails.html", **content)

        # return redirect(url_for('app1.stage_offspring', **request.form.to_dict()))

    content = request.args.to_dict()                    # get request arguments
    content['prefix'] = db.get_prefix(current_user.id)  # get flock prefix of owner
    content['colors'] = db.get_colors(with_id=True, with_default=True)     # get farm tag colors
    content['official_locations'] = db.get_locations(with_default=True, official_only=True)           # get locations of the official tags
    content['farm_locations'] = db.get_locations(with_default=True, farm_only=True)
    content['types'] = db.get_types(with_id=True, with_default=True)       # get types of farm tags
    content['numberBorn'] = int(content['numberBorn'])  # get number of born as an integer (ignore pycharm err)
    content['coat_colors'] = db.get_coat_colors()       # get coat colors

    # parse out the sire_id and the sire name
    old_sire = content['sire'].split(",")
    session['sire_birth_id'] = old_sire[0]
    content['sire'] = " ".join(old_sire[1:])

    # parse out the sire_id and the dam name
    old_dam = content['dam'].split(",")
    session['dam_birth_id'] = old_dam[0]
    content['dam'] = " ".join(old_dam[1:])

    # get the conception type
    # conception_types = db.get_conception_types()
    conception_id = content['conceptionType']
    session['conception_type_id'] = conception_id
    content['conceptionType'] = db.get_conception_type(content['conceptionType'])

    session['birth_notes'] = content['birth_notes']

    return render_template("/birthing/offspringdetails.html", **content)


@app1.route("/pedigree/<animal_id>")
@app1.route("/pedigree/<animal_id>", defaults={"flock_prefix": None})
@login_required
def pedigree(animal_id, flock_prefix):

    max_depth = 4  # This should stay as 4, this represents how deep to make the pedigree chart
                   # note that if this exceeds 4 you may see some weird formatting on the frontend

    ans = db.animal_pedigree(animal_id, depth=4)  # depth is the number of generations down to go! (don't make it too big!)

    offspring_of_animal = db.get_offspring_data_of(animal_id)

    content = dict()

    content["offspring_of_animal"] = offspring_of_animal

    # oa stands for original animal
    # after the underscore it represents sires and dams following.
    # for example 'oa_sds' represents the original animals sire's dam's sire

    fp = ans['flock_prefix']
    content['oa'] = fp + " " + ans['name']
    content['oa_rn'] = ans['reg_num']
    content['oa_dob'] = ans['birth_date']
    content['oa_id'] = animal_id
    content['oa_sex_id'] = ans['sex_id']

    fp = ans['parents']['sire']['flock_prefix']
    content['oa_s'] = fp + " " + ans['parents']['sire']['name']
    content['oa_s_rn'] = ans['parents']['sire']['reg_num']
    content['oa_s_dob'] = ans['parents']['sire']['birth_date']
    content['oa_s_id'] = ans['parents']['sire']['animal_id']

    fp = ans['parents']['dam']['flock_prefix']
    content['oa_d'] = fp + " " + ans['parents']['dam']['name']
    content['oa_d_rn'] = ans['parents']['dam']['reg_num']
    content['oa_d_dob'] = ans['parents']['dam']['birth_date']
    content['oa_d_id'] = ans['parents']['dam']['animal_id']

    if max_depth >= 3:
        fp = ans['parents']['sire']['parents']['sire']['flock_prefix']
        content['oa_ss'] = fp + " " + ans['parents']['sire']['parents']['sire']['name']
        content['oa_ss_rn'] = ans['parents']['sire']['parents']['sire']['reg_num']
        content['oa_ss_dob'] = ans['parents']['sire']['parents']['sire']['birth_date']
        content['oa_ss_id'] = ans['parents']['sire']['parents']['sire']['animal_id']

        fp = ans['parents']['sire']['parents']['dam']['flock_prefix']
        content['oa_sd'] = fp + " " + ans['parents']['sire']['parents']['dam']['name']
        content['oa_sd_rn'] = ans['parents']['sire']['parents']['dam']['reg_num']
        content['oa_sd_dob'] = ans['parents']['sire']['parents']['dam']['birth_date']
        content['oa_sd_id'] = ans['parents']['sire']['parents']['dam']['animal_id']

        fp = ans['parents']['dam']['parents']['sire']['flock_prefix']
        content['oa_ds'] = fp + " " + ans['parents']['dam']['parents']['sire']['name']
        content['oa_ds_rn'] = ans['parents']['dam']['parents']['sire']['reg_num']
        content['oa_ds_dob'] = ans['parents']['dam']['parents']['sire']['birth_date']
        content['oa_ds_id'] = ans['parents']['dam']['parents']['sire']['animal_id']

        fp = ans['parents']['dam']['parents']['dam']['flock_prefix']
        content['oa_dd'] = fp + " " + ans['parents']['dam']['parents']['dam']['name']
        content['oa_dd_rn'] = ans['parents']['dam']['parents']['dam']['reg_num']
        content['oa_dd_dob'] = ans['parents']['dam']['parents']['dam']['birth_date']
        content['oa_dd_id'] = ans['parents']['dam']['parents']['dam']['animal_id']

    if max_depth >= 4:
        fp = ans['parents']['sire']['parents']['sire']['parents']['sire']['flock_prefix']
        content['oa_sss'] = fp + " " + ans['parents']['sire']['parents']['sire']['parents']['sire']['name']
        content['oa_sss_rn'] = ans['parents']['sire']['parents']['sire']['parents']['sire']['reg_num']
        content['oa_sss_dob'] = ans['parents']['sire']['parents']['sire']['parents']['sire']['birth_date']
        content['oa_sss_id'] = ans['parents']['sire']['parents']['sire']['parents']['sire']['animal_id']

        fp = ans['parents']['sire']['parents']['sire']['parents']['dam']['flock_prefix']
        content['oa_ssd'] = fp + " " + ans['parents']['sire']['parents']['sire']['parents']['dam']['name']
        content['oa_ssd_rn'] = ans['parents']['sire']['parents']['sire']['parents']['dam']['reg_num']
        content['oa_ssd_dob'] = ans['parents']['sire']['parents']['sire']['parents']['dam']['birth_date']
        content['oa_ssd_id'] = ans['parents']['sire']['parents']['sire']['parents']['dam']['animal_id']

        fp = ans['parents']['sire']['parents']['dam']['parents']['sire']['flock_prefix']
        content['oa_sds'] = fp + " " + ans['parents']['sire']['parents']['dam']['parents']['sire']['name']
        content['oa_sds_rn'] = ans['parents']['sire']['parents']['dam']['parents']['sire']['reg_num']
        content['oa_sds_dob'] = ans['parents']['sire']['parents']['dam']['parents']['sire']['birth_date']
        content['oa_sds_id'] = ans['parents']['sire']['parents']['dam']['parents']['sire']['animal_id']

        fp = ans['parents']['sire']['parents']['dam']['parents']['dam']['flock_prefix']
        content['oa_sdd'] = fp + " " + ans['parents']['sire']['parents']['dam']['parents']['dam']['name']
        content['oa_sdd_rn'] = ans['parents']['sire']['parents']['dam']['parents']['dam']['reg_num']
        content['oa_sdd_dob'] = ans['parents']['sire']['parents']['dam']['parents']['dam']['birth_date']
        content['oa_sdd_id'] = ans['parents']['sire']['parents']['dam']['parents']['dam']['animal_id']

        fp = ans['parents']['dam']['parents']['sire']['parents']['sire']['flock_prefix']
        content['oa_dss'] = fp + " " + ans['parents']['dam']['parents']['sire']['parents']['sire']['name']
        content['oa_dss_rn'] = ans['parents']['dam']['parents']['sire']['parents']['sire']['reg_num']
        content['oa_dss_dob'] = ans['parents']['dam']['parents']['sire']['parents']['sire']['birth_date']
        content['oa_dss_id'] = ans['parents']['dam']['parents']['sire']['parents']['sire']['animal_id']

        fp = ans['parents']['dam']['parents']['sire']['parents']['dam']['flock_prefix']
        content['oa_dsd'] = fp + " " + ans['parents']['dam']['parents']['sire']['parents']['dam']['name']
        content['oa_dsd_rn'] = ans['parents']['dam']['parents']['sire']['parents']['dam']['reg_num']
        content['oa_dsd_dob'] = ans['parents']['dam']['parents']['sire']['parents']['dam']['birth_date']
        content['oa_dsd_id'] = ans['parents']['dam']['parents']['sire']['parents']['dam']['animal_id']

        fp = ans['parents']['dam']['parents']['dam']['parents']['sire']['flock_prefix']
        content['oa_dds'] = fp + " " + ans['parents']['dam']['parents']['dam']['parents']['sire']['name']
        content['oa_dds_rn'] = ans['parents']['dam']['parents']['dam']['parents']['sire']['reg_num']
        content['oa_dds_dob'] = ans['parents']['dam']['parents']['dam']['parents']['sire']['birth_date']
        content['oa_dds_id'] = ans['parents']['dam']['parents']['dam']['parents']['sire']['animal_id']

        fp = ans['parents']['dam']['parents']['dam']['parents']['dam']['flock_prefix']
        content['oa_ddd'] = fp + " " + ans['parents']['dam']['parents']['dam']['parents']['dam']['name']
        content['oa_ddd_rn'] = ans['parents']['dam']['parents']['dam']['parents']['dam']['reg_num']
        content['oa_ddd_dob'] = ans['parents']['dam']['parents']['dam']['parents']['dam']['birth_date']
        content['oa_ddd_id'] = ans['parents']['dam']['parents']['dam']['parents']['dam']['animal_id']

    return render_template("pedigree/pedigree.html", **content)


@app1.route("/registrations/", methods=('GET', 'POST'))
@login_required
def registrations():
    content = dict()
    if request.method == 'POST':
        form_data = request.form.to_dict()
        checked = [q for q in form_data.keys() if q.startswith("cb")]

        if len(checked) == 0:
            # reshow the registration table
            session['birth_notified_animals'] = db.get_birth_notified_animals(current_user.id)
            content['birth_notified_animals'] = session['birth_notified_animals']
            content['coat_colors'] = db.get_coat_colors()  # get coat colors
            flash("You must select at least 1 animal before proceeding")
            return render_template("/registrations/registration.html", **content)


        # prefix_animal_id = "animal_id_"
        prefix_flock = "prefix_"
        prefix_name = "name_"
        prefix_dob = "dob_"
        prefix_sex = "sex_"
        prefix_birth_type = "birth_type_"
        prefix_reg_num = "reg_num_"
        prefix_coat_color = "coat_color_"
        prefix_id_number = "id_number_"
        prefix_id_animalid = "id_animalid_"

        registration_data = list()

        for cb in checked:

            num = cb.split("_")[1]
            # animal_id = form_data[f"{prefix_animal_id}{num}"]
            flock = form_data[f"{prefix_flock}{num}"]
            name = form_data[f"{prefix_name}{num}"]
            dob = form_data[f"{prefix_dob}{num}"]
            sex = form_data[f"{prefix_sex}{num}"]
            birth_type = form_data[f"{prefix_birth_type}{num}"]
            reg_num = form_data[f"{prefix_reg_num}{num}"]
            coat_color = form_data[f"{prefix_coat_color}{num}"]
            id_number = form_data[f"{prefix_id_number}{num}"]
            id_animalid = form_data[f"{prefix_id_animalid}{num}"]

            # row_data = [animal_id, flock, name, dob, sex, birth_type, reg_num]
            row_data = [flock, name, dob, sex, birth_type, reg_num, coat_color, id_number, id_animalid]

            registration_data.append(row_data)

        session['registrations'] = registration_data

        return redirect(url_for("app1.registration_update"))

    session['birth_notified_animals'] = db.get_birth_notified_animals(current_user.id)  # FIXME this needs to be the member number
    content['birth_notified_animals'] = session['birth_notified_animals']
    content['coat_colors'] = db.get_coat_colors()  # get coat colors

    return render_template("/registrations/registration.html", **content)


@app1.route("/registration_update/", methods=('GET', 'POST'))
@login_required
def registration_update():

    content = dict()

    if request.method == 'POST':

        form_data = request.form.to_dict()

        cont = True
        idx = 1
        while cont:
            try:
                form_data[f'color_{idx}'] = form_data[f'color_{idx}']
                form_data[f'color_name_{idx}'] = db.get_color(form_data[f'color_{idx}'])

                form_data[f'type_{idx}'] = form_data[f'type_{idx}']
                form_data[f'type_name_{idx}'] = db.get_tag_type_name(form_data[f'type_{idx}'])

                form_data[f'location_name_{idx}'] = form_data[f'location_{idx}']
                form_data[f'location_key_{idx}'] = db.get_tag_loc_key(form_data[f'location_name_{idx}'])

                form_data[f'ft_color_{idx}'] = form_data[f'ft_color_{idx}']
                form_data[f'ft_color_name_{idx}'] = db.get_color(form_data[f'ft_color_{idx}'])

                form_data[f'ft_type_{idx}'] = form_data[f'ft_type_{idx}']
                form_data[f'ft_type_name_{idx}'] = db.get_tag_type_name(form_data[f'ft_type_{idx}'])

                form_data[f'ft_location_name_{idx}'] = form_data[f'ft_location_{idx}']
                form_data[f'ft_location_key_{idx}'] = db.get_tag_loc_key(form_data[f'ft_location_name_{idx}'])


                if f"is_official_cb_{idx}" in form_data.keys():
                    form_data[f'is_official_{idx}'] = 1
                else:
                    form_data[f'is_official_{idx}'] = 0

                form_data[f'coat_color_key_{idx}'] = db.get_coat_color_key(form_data[f"coat_color_{idx}"])

                idx += 1
            except KeyError:
                cont = False


        result = stage.stage_registrations(current_user, form_data)

        if result:
            # good!
            flash(
                "Data successfully submitted. Waiting for registrar approval. No further action from you is required.")
            return redirect(url_for("app1.home"))
        else:
            return redirect(url_for('app1.registration_update'))

    animal_data = session['registrations']
    # content['animal_data'] = animal_data

    # TODO add actual tag info to animal_data
    # index 8 has the id_animalid

    # new_animal_data = []

    for row in animal_data:
        id_animalid = row[8]

        ans = db.get_id_info(id_animalid)

        if len(ans) > 0:
            # animal has tags, create tag data

            tag_dict = dict()

            for _id, animal_name, id_number, id_location_name, id_color_name, id_type_name, official_id in ans:

                id_type_check = id_type_name.lower()
                is_official_tag = official_id == 1 or official_id == "1"
                print("MITCH DEBUG!!")
                print(id_type_name)
                if is_official_tag:
                    tag_dict['official'] = {"type": id_type_name, "color": id_color_name, "loc": id_location_name, "number": id_number}
                elif id_type_check in ["farm", "electronic", "freeze brand", "tattoo"] and "unofficial" not in tag_dict.keys():
                    tag_dict['unofficial'] = {"type": id_type_name, "color": id_color_name, "loc": id_location_name, "number": id_number}

            # at the end add the tag dictionary
            row.append(tag_dict)
        else:
            row.append(dict()) # append empty dictionary as placeholder so templating is possible

    content['animal_data'] = animal_data

    content['colors'] = db.get_colors(with_id=True)
    content['locations'] = db.get_locations()
    content['reasons'] = db.get_id_remove_reasons()
    content['types'] = db.get_types(with_id=True)
    content['sexes'] = db.get_sexes()
    content['coat_colors'] = db.get_coat_colors()    # get coat colors
    content['birth_types'] = db.get_birth_types()    # get birth types

    return render_template("/registrations/update_registrations.html", **content)


@app1.route("/stage_deaths/", methods=('GET', 'POST'))
@login_required
def stage_deaths():
    #req_dict = request.form.to_dict()
    req_list = session['death_staging']

    result = stage.staging_deaths_file_creation(current_user, req_list)

    if result:
        # good!
        flash("Data successfully submitted. Waiting for registrar approval. No further action from you is required.")
        return redirect(url_for("app1.home"))
    else:
        return "fix this please"  # FIXME: what to return when it fails?


@app1.route("/stage_id_updates/", methods=('GET', 'POST'))
@login_required
def stage_id_updates():
    content = request.form.to_dict()

    if request.method == 'POST':
        # check that all dates are filled
        cont = True
        i = 1
        all_data_filled = True
        # continue while checking next rows and all data is still filled in thus far
        while cont and all_data_filled:
            # check specific row
            if f"old_name_{i}" in content.keys() and f"new_name_{i}" in content.keys():
                # make sure data is entered
                if content[f"new_date_on_{i}"] == "" or \
                        content[f"old_date_off_{i}"] == "" or \
                        content[f"new_tagnum_{i}"] == "":
                    all_data_filled = False
                elif content[f"old_reason_{i}"] == "--Select Reason--":
                    flash("Please select an update reason.")
                    return redirect(url_for('app1.id_update_verify', **content, _method='GET'))
                i += 1
            else:
                # checking is done
                cont = False

        # check if all data has been filled out
        if all_data_filled:
            result = stage.staging_id_update_file_creation(current_user, data=content)
            # if CSV was saved, then return home with flashed message
            if result:
                # good!
                flash("Data successfully submitted. Waiting for registrar approval. "
                      "No further action from you is required.")
                return redirect(url_for("app1.home"))
            else:
                # bad!
                flash("There was an error processing your request. Please contact an administrator.")
                return redirect(url_for('app1.id_update_verify', **content, _method='GET'))  # OOGIECHECK - what do if CSV saving fails?

        else:
            flash("Please ensure all required information is filled out.")
            return redirect(url_for('app1.id_update_verify', **content, _method='GET'))

    return "fix this please"


# @app1.route("/stage_offspring/", methods=('GET', 'POST'))
# @login_required
# def stage_offspring():
#     req_dict = request.args.to_dict()
#
#     result = stage.staging_offspring_file_creation(current_user, req_dict)
#
#     if result:
#         # good!
#         flash("Data successfully submitted. Waiting for registrar approval. No further action from you is required.")
#         return redirect(url_for("app1.home"))
#     else:
#         # get old data user input so they don't have to reenter it
#         content = dict()
#         content['numberBorn'] = int(req_dict['numberBorn'])
#         content['dob'] = req_dict['dob']
#         content['dam'] = req_dict['dam']
#         content['sire'] = req_dict['sire']
#         content['conceptionType'] = req_dict['conceptionType']
#
#         content['colors'] = db.get_colors()
#         content['locations'] = db.get_locations()
#         content['types'] = db.get_types()
#         content['prefix'] = db.get_prefix(current_user.id)
#
#         flash("Submission Failed. Please verify that all data is present")
#         # render same template again
#         return render_template("/birthing/offspringdetails.html", **content)
#
#     #return str(request.args)


@app1.route("/transfers/", methods=('GET', 'POST'))
@login_required
def transfers():
    content = dict()
    if request.method == 'POST':
        data = request.form.to_dict()
        # animal_id, reg_num, prefix, name, official_tag, sex, birth_type, dob, coat_color
        # many keys from previous page titled {{ form_name }}_{{ num }}
        # such as "name_6"

        cont = True
        i = 1
        while cont:
            if f"name_{i}" in data.keys():
                i += 1
            else:
                cont = False


        # get prefixes
        animal_id_prefix = "animal_id_"
        reg_num_prefix = "reg_num_"
        flock_prefix_prefix = "prefix_"
        name_prefix = "name_"
        dob_prefix = "dob_"
        birth_type_prefix = "birth_type_"
        sex_prefix = "sex_"
        coat_color_prefix = "coat_color_"

        selected_animals = list()

        # NOTE: this for loop uses j as a counting variable instead of i !!!!
        for j in range(1, i):

            if f"cb_{j}" in data.keys():
                animal_id = data[f"{animal_id_prefix}{j}"]
                reg_num = data[f"{reg_num_prefix}{j}"]
                flock = data[f"{flock_prefix_prefix}{j}"]
                name = data[f"{name_prefix}{j}"]
                dob = data[f"{dob_prefix}{j}"]
                birth_type = data[f"{birth_type_prefix}{j}"]
                sex = data[f"{sex_prefix}{j}"]
                coat_color = data[f"{coat_color_prefix}{j}"]

                row = (animal_id, reg_num, flock, name, dob, birth_type, sex, coat_color,)
                selected_animals.append(row)

        session['transferred_animals'] = selected_animals

        # get the buy if it exists
        test_pref = "buyer_"
        res = {key: val for key, val in data.items()
               if key.startswith(test_pref)}

        if len(res.keys()) != 1:
            # buyer was not selected
            if "buyer" in session.keys():
                del session['buyer']
            return redirect(url_for("app1.transfers_verify", animals=selected_animals))
        else:
            # not the best way to get the value from a dictionary but it works i guess
            buyer = None
            for k, v in res.items():
                buyer = k

        buyer_num = buyer.split("_")[1]

        # get the buyer information
        b_memnum = data[f'memnum_{buyer_num}']
        b_first_name = data[f'fn_{buyer_num}']
        b_last_name = data[f'ln_{buyer_num}']
        b_region = data[f'region_{buyer_num}']
        buyer_data = (b_memnum, b_first_name, b_last_name, b_region)

        session['buyer'] = buyer_data

        return redirect(url_for("app1.transfers_verify", animals=selected_animals))


    if current_user.contact_id != "":
        content['animals'] = db.all_alive_registered_animals_owned_by(contact_id=current_user.contact_id, get_coat_color=True)
    elif current_user.company_id != "":
        content['animals'] = db.all_alive_registered_animals_owned_by(company_id=current_user.company_id, get_coat_color=True)

    content['members'] = db.get_current_members()

    return render_template("/transfers/transfer.html", **content)


@app1.route("/transfers_verify/", methods=('GET', 'POST'))
@login_required
def transfers_verify():
    content = dict()

    # handle staging data once posted
    if request.method == 'POST':
        # get data out of page and submit it

        data = request.form.to_dict()

        buyer_contact_id = None
        buyer_company_id = None

        # get buyer's contact_id and company_id
        if "buyer_memnum" in data.keys():
            buyer_memnum = data["buyer_memnum"]
            contact_ans = db.get_contact_id_from_member(buyer_memnum)
            print(contact_ans)
            if contact_ans is not None and len(contact_ans) > 0:
                buyer_contact_id = contact_ans[0][0]
            company_ans = db.get_company_id_from_member(buyer_memnum)
            if company_ans is not None and len(company_ans) > 0:
                buyer_company_id = company_ans[0][0]

        data["buyer_prem"]="NONE"
        if buyer_contact_id is not None:
            data["buyer_prem"] = db.get_premise_id_by_contact(buyer_contact_id)
        elif buyer_company_id is not None:
            data["buyer_prem"] = db.get_premise_id_by_company(buyer_company_id)

        data["seller_prem"] = "NONE"
        if current_user.contact_id is not None:
            data["seller_prem"] = db.get_premise_id_by_contact(current_user.contact_id)
        elif current_user.contact_id is not None:
            data["seller_prem"] = db.get_premise_id_by_company(current_user.contact_id)

        ans = stage.stage_transfers(current_user, data, buyer_contact_id=buyer_contact_id, buyer_company_id=buyer_company_id)

        if ans:
            flash("Data uploaded successfully. No further action from you is required.")
            return redirect(url_for("app1.home"))
        else:
            return redirect(url_for('app1.transfers_verify'))


    content['animals'] = session['transferred_animals']
    content['animals_length'] = len(content['animals'])

    if 'buyer' in session.keys():
        content['buyer'] = session['buyer']
    else:
        content["states"] = [x[1] for x in db.get_states()]

    return render_template("/transfers/transfer_verify.html", **content)


# - [ ] Show a table of the animals with the reg num,  prefix, name, birthdate, birth type, sex and coat color
# - [ ] Under that show the date sold and ask for a date moved to new location
# - [ ] Under that show the contact info for the buyer
# - [ ] Member Number, First and Last Name and Company, then 2 sets of data, their physical address is required but
#       if there is a mailing address it will also be displayed. Address consists of address line 1, address line 2,
#       City, State or province form the database dropdown, post code. All those are mandatory. Then the optional info:
#       Federal Scrapie Flock ID, Federal Premise ID, State Premise ID, premise latitude, premise longitude,
#       primary phone, mobile phone, primary email and web site.
# - [ ] If no member was selected then ask for a physical and mailing address per above (it can be the same) and send
#       that data down as well in a separate .CSV file Note that the member number will be assigned by the registrar.